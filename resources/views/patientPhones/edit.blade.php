@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientPhones, ['route' => ['patientPhones.update', $patientPhones->id], 'method' => 'patch']) !!}

        @include('patientPhones.fields')

    {!! Form::close() !!}
</div>
@endsection
