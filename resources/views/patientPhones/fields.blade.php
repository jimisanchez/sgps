<!--- Patient Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('patient_id', 'Patient Id:') !!}
    {!! Form::text('patient_id', null, ['class' => 'form-control']) !!}
</div>

<!--- Phonenumber Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('phoneNumber', 'Phonenumber:') !!}
    {!! Form::text('phoneNumber', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 col-lg-4">
{!! Form::label('type_id', 'Type:') !!}
{!! Form::select('type_id', $types, null, ['class' => 'form-control']) !!}
</div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>