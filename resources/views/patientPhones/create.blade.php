@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'patientPhones.store']) !!}

        @include('patientPhones.fields')

    {!! Form::close() !!}
</div>
@endsection
