@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">PatientPhones</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('patientPhones.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($patientPhones->isEmpty())
                <div class="well text-center">No PatientPhones found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Patient Id</th>
			<th>Phonenumber</th>
                                    <th>Type</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($patientPhones as $patientPhones)
                        <tr>
                            <td><a href="/patients/{!! $patientPhones->patient_id !!}/">{!! $patientPhones->patient->firstName !!} {!! $patientPhones->patient->lastName !!}</a></td>
		    <td><a href="/patientPhones/{!! $patientPhones->id !!}/edit">{!! $patientPhones->phoneNumber !!}</a></td>
                            <td>{!! $patientPhones->type->description !!}</td>
                            <td>
                                <a href="{!! route('patientPhones.edit', [$patientPhones->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('patientPhones.delete', [$patientPhones->id]) !!}" onclick="return confirm('Are you sure wants to delete this PatientPhones?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>
@endsection