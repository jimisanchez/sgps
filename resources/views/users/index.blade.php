@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Users</h1>
            @if (Entrust::can('create-user') )
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('users.create') !!}">Add New</a>
            @endif
        </div>

        <div class="row">
            @if($users->isEmpty())
                <div class="well text-center">No Users found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Name</th>
			<th>Email</th>
			<th>Default Organization</th>
                                    <th>Role</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{!! $user->name !!}</td>
                            <td>{!! $user->email !!}</td>
                            <td>{!! $user->currentOrganization->name !!}</td>
                            <td>
                                @foreach($user->roles as $role)
                                    {!! $role->display_name !!} <br/>
                                @endforeach
                            </td>
                            <td>
                                @if (Entrust::can('edit-user') )
                                 <a href="{!! route('users.edit', [$user->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                @endif
                                @if ( Auth::id() != $user->id)
                                    @if( Entrust::can('delete-user') )
                                        <a href="{!! route('users.delete', [$user->id]) !!}" onclick="return confirm('Are you sure wants to delete this User?')"><i class="glyphicon glyphicon-remove"></i></a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>
@endsection