<!--- Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm6 col-lg-4">
    {!! Form::label('name','Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!--- Current Org Id Field 
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('current_org_id', 'Current Org Id:') !!}
    {!! Form::text('current_org_id', null, ['class' => 'form-control']) !!}
</div>--->

<!--- Roles --->
@if ($user->id != \Auth::user()->id )
<div class="form-group col-sm-6 col-lg-4">
{!! Form::label('roles', 'Roles:') !!}
{!! Form::select('roles[]', $roles, $user->roles->lists('id'), ['class' => 'form-control']) !!}
</div>
@endif

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
