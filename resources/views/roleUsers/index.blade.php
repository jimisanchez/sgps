@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">RoleUsers</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('roleUsers.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($roleUsers->isEmpty())
                <div class="well text-center">No RoleUsers found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>User Id</th>
			<th>Role Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($roleUsers as $roleUser)
                        <tr>
                            <td>{!! $roleUser->user_id !!}</td>
					<td>{!! $roleUser->role_id !!}</td>
                            <td>
                                <a href="{!! route('roleUsers.edit', [$roleUser->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('roleUsers.delete', [$roleUser->id]) !!}" onclick="return confirm('Are you sure wants to delete this RoleUser?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection