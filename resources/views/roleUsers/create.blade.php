@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'roleUsers.store']) !!}

        @include('roleUsers.fields')

    {!! Form::close() !!}
</div>
@endsection
