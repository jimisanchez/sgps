@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($roleUser, ['route' => ['roleUsers.update', $roleUser->id], 'method' => 'patch']) !!}

        @include('roleUsers.fields')

    {!! Form::close() !!}
</div>
@endsection
