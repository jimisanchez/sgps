<!--- Patient Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('patient_id', 'Patient Id:') !!}
    {!! Form::text('patient_id', null, ['class' => 'form-control']) !!}
</div>

<!--- Organization Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('organization_id', 'Organization Id:') !!}
    {!! Form::text('organization_id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
