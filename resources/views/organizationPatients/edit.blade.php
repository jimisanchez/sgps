@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($organizationPatients, ['route' => ['organizationPatients.update', $organizationPatients->id], 'method' => 'patch']) !!}

        @include('organizationPatients.fields')

    {!! Form::close() !!}
</div>
@endsection
