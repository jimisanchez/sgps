@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'organizationPatients.store']) !!}

        @include('organizationPatients.fields')

    {!! Form::close() !!}
</div>
@endsection
