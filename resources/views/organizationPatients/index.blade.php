@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">OrganizationPatients</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('organizationPatients.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($organizationPatients->isEmpty())
                <div class="well text-center">No OrganizationPatients found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Patient Id</th>
			<th>Organization Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($organizationPatients as $organizationPatients)
                        <tr>
                            <td><a href="/patients/{!! $organizationPatients->patient_id !!}/edit">{!! $organizationPatients->patient->firstName !!} {!! $organizationPatients->patient->lastName !!}</a></td>
					<td>{!! $organizationPatients->organization->name !!}</td>
                            <td>
                                <a href="{!! route('organizationPatients.edit', [$organizationPatients->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('organizationPatients.delete', [$organizationPatients->id]) !!}" onclick="return confirm('Are you sure wants to delete this OrganizationPatients?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>
@endsection