<!--- Patient Id Field 
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('patient_id', 'Patient Id:') !!}
   --> 
    {!! Form::hidden('patient_id', null, ['class' => 'form-control']) !!}
<!-- </div> -->

<!--- Street Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('street', 'Street:') !!}
    {!! Form::text('street', null, ['class' => 'form-control']) !!}
</div>

<!--- Secondary Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('secondary', 'Secondary:') !!}
    {!! Form::text('secondary', null, ['class' => 'form-control']) !!}
</div>

<!--- City --->
<div class="form-group-col-sm-6 col-lg-4">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!--- Postal Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('postal', 'Postal:') !!}
    {!! Form::text('postal', null, ['class' => 'form-control']) !!}
</div>

<!--- State Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('state', 'State:') !!}
    {!! Form::select('state_id', $states, null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
