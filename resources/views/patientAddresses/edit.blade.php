@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientAddress, ['route' => ['patientAddresses.update', $patientAddress->id], 'method' => 'patch']) !!}

        @include('patientAddresses.fields')

    {!! Form::close() !!}
</div>
@endsection
