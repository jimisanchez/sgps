@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">PatientAddresses</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('patientAddresses.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($patientAddresses->isEmpty())
                <div class="well text-center">No PatientAddresses found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Patient Id</th>
			<th>Street</th>
			<th>Secondary</th>
                                    <th>City</th>
			<th>Postal</th>
			<th>State</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($patientAddresses as $patientAddress)
                        <tr>
                            <td><a href="/patients/{!! $patientAddress->patient->id !!}">{!! $patientAddress->patient->firstName !!} {!! $patientAddress->patient->lastName !!}</a></td>
					<td>{!! $patientAddress->street !!}</td>
					<td>{!! $patientAddress->secondary !!}</td>
                                                            <td>{!! $patientAddress->city !!}</td>
					<td>{!! $patientAddress->postal !!}</td>
					<td>{!! $patientAddress->state->state_name !!}</td>
                            <td>
                                <a href="{!! route('patientAddresses.edit', [$patientAddress->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('patientAddresses.delete', [$patientAddress->id]) !!}" onclick="return confirm('Are you sure wants to delete this PatientAddress?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>
@endsection