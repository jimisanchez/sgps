@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientAddress, ['route' => 'patientAddresses.store']) !!}

        @include('patientAddresses.fields')

    {!! Form::close() !!}
</div>
@endsection
