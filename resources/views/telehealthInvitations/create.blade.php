@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'telehealthInvitations.store']) !!}

        @include('telehealthInvitations.fields')

    {!! Form::close() !!}
</div>
@endsection
@section('scripts')
<link href="/css/datetimepicker.css" rel="stylesheet" />
<script src="/js/plugins/jquery.datetimepicker.js"></script>
<script>
$(document).ready( function() {
        $( "#sessionStartTime" ).datetimepicker();
        $( "#sessionEndTime" ).datetimepicker();

});
</script>
@endsection