@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Telehealth Invitations</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('telehealthInvitations.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($telehealthInvitations->isEmpty())
                <div class="well text-center">No TelehealthInvitations found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Email</th>
			<th>Sessionhash</th>
			<th>Start Time</th>
			<th>End Time</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($telehealthInvitations as $telehealthInvitation)
                        <tr>
                            <td>{!! $telehealthInvitation->email !!}</td>
                        
					<td>{!! $telehealthInvitation->sessionHash !!}</td>
					<td>{!! $telehealthInvitation->sessionStartTime !!}</td>
					<td>{!! $telehealthInvitation->sessionEndTime !!}</td>
                            <td>
                                <a href="{!! route('telehealthInvitations.edit', [$telehealthInvitation->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('telehealthInvitations.delete', [$telehealthInvitation->id]) !!}" onclick="return confirm('Are you sure wants to delete this TelehealthInvitation?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection