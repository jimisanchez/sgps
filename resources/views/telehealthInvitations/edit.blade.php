@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($telehealthInvitation, ['route' => ['telehealthInvitations.update', $telehealthInvitation->id], 'method' => 'patch']) !!}

        @include('telehealthInvitations.fields')

    {!! Form::close() !!}
</div>
@endsection
