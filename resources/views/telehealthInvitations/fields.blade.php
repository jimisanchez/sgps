<!--- Email Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!--- Sessionstarttime Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sessionStartTime', 'Session Start Time:') !!}
    {!! Form::text('sessionStartTime', null, ['class' => 'form-control']) !!}
</div>

<!--- Sessionendtime Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('sessionEndTime', 'Session End Time:') !!}
    {!! Form::text('sessionEndTime', null, ['class' => 'form-control']) !!}
</div>

<!--- User Id --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('user_id', "User") !!}
    {!! Form::select('user_id', $users, null, ['class' => 'form-control']) !!}
</div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
