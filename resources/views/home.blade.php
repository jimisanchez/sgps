@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>	
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					@if(Session::has('success'))
					    <div class="alert alert-success">
					        <h2>{{ Session::get('success') }}</h2>
					    </div>
					@endif
				<div class="panel-body">
					You are logged in! <br/>
					@if ( count($organizations) > 1 )
					<form action="/home/index"  method="post">
						Choose Organization: 
						{!! Form::select('organization', $organizations, $user->current_org_id); !!}
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<input type="submit" value="Choose" />
					</form>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
