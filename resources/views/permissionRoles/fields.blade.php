<!--- Permission Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('permission_id', 'Permission:') !!}
    {!! Form::select('permission_id', $permissions, null, ['class' => 'form-control']) !!}
</div>

<!--- Role Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('role_id', 'Role:') !!}
    {!! Form::select('role_id',  $roles, null, ['class' => 'form-control']) !!}
</div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
