@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($permissionRole, ['route' => ['permissionRoles.update', $permissionRole->id], 'method' => 'patch']) !!}

        @include('permissionRoles.fields')

    {!! Form::close() !!}
</div>
@endsection
