@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'permissionRoles.store']) !!}

        @include('permissionRoles.fields')

    {!! Form::close() !!}
</div>
@endsection
