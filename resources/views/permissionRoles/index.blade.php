@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">PermissionRoles</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('permissionRoles.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($permissionRoles->isEmpty())
                <div class="well text-center">No PermissionRoles found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Permission</th>
			<th>Role Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($permissionRoles as $permissionRole)
                        <tr>
                            <td>{!! $permissionRole->permission->display_name !!}</td>
					<td>{!! $permissionRole->role->display_name !!}</td>
                            <td>
                                <a href="{!! route('permissionRoles.edit', [$permissionRole->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('permissionRoles.delete', [$permissionRole->id]) !!}" onclick="return confirm('Are you sure wants to delete this PermissionRole?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection