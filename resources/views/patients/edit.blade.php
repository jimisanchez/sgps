@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patient, ['route' => ['patients.update', $patient->id], 'method' => 'patch']) !!}

        @include('patients.fields')

    {!! Form::close() !!}
</div>
@endsection
