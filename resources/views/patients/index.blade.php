@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Patients</h1>
            @if (Entrust::can('create-patient'))
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('patients.create') !!}">Add New</a>
            @endif
        </div>

        <div class="row">
            @if($patients->isEmpty())
                <div class="well text-center">No Patients found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Birthdate</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($patients as $patient)
                        <tr>
                            <td><a href="/patients/{!! $patient->patient_id !!}/">{!! $patient->firstName !!}</a></td>
					<td>{!! $patient->lastName !!}</td>
					<td>{!! $patient->birthDate !!}</td>
					<td>{!! $patient->gender->description !!}</td>
                                                            <td>
                                                                @foreach($patient->addresses as $address) 
                                                                    <small><a href="/patientAddresses/{!! $address->id !!}/edit">{!! $address->street !!} {!! $address->secondary !!}
                                                                    {!! $address->city !!}, {!! $address->state->state_abbreviation !!}, {!! $address->postal !!}</a></small>
                                                                    <br/>
                                                                @endforeach

                                                            <td>
                                                                @foreach($patient->phones as $phone)
                                                                    <small><a href="/patientPhones/{!! $phone->id !!}/edit">{!! $phone->phoneNumber !!}</a></small>
                                                                @endforeach
                                                            </td>
                                                            <td>
                                                                {!! $patient->email !!}
                                                            </td>
                            <td>
                                @if(!$patient->deleted_at)
                                    @if (Entrust::can('edit-patient'))
                                        <a href="{!! route('patients.edit', [$patient->patient_id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                    @endif
                                    @if (Entrust::can('delete-patient')) 
                                        <a href="{!! route('patients.delete', [$patient->patient_id]) !!}" onclick="return confirm('Are you sure wants to delete this Patient?')"><i class="glyphicon glyphicon-remove"></i></a>
                                    @endif
                                @endif                                
                                @if($patient->deleted_at)
                                    <a href="{!! route('patients.restore', [$patient->patient_id]) !!}" title="Undelete"><i class="glyphicon glyphicon-refresh"></i></a>
                                @endif
                                @if($patient->email)
                                    <a href="#" title="telehealth"><li class="glyphicon glyphicon-hd-video"></li></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
        {!! $patients->render() !!}
    </div>
@endsection