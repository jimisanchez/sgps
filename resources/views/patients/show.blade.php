@extends('app')

@section('content')
<div class="container">
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Patient Demographics</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 col-md-4">
                    Name: {!! $patient->firstName !!} {!! $patient->middleName !!} {!! $patient->lastName !!} <a href="/patients/{!! $patient->id !!}/edit"><i class="glyphicon glyphicon-edit"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-4">
                                Birth Date: {!! $patient->birthDate !!}
                </div>
            </div>
        </div>
    </div>
    
    
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Addresses <a class="pull-right btn btn-xs btn-primary" href="{!! route('patientAddresses.create',$patient->id) !!}">Add New</a></h3>
  </div>
  <div class="panel-body">
        @foreach($patient->addresses as $address)
        <div class="row">
            <div class="col-xs-6 col-md-4">
                {!! $address->street !!} {!! $address->secondary !!} {!! $address->postal !!} {!! $address->state->state_name !!}
            </div>
            <div class="col-xs-6 col-md-4">
                <a href="{!! route('patientAddresses.edit', [$address->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('patientAddresses.delete', [$address->id]) !!}" onclick="return confirm('Are you sure wants to delete this address?')"><i class="glyphicon glyphicon-remove"></i></a>
            </div>
        </div>
        @endforeach
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Phones <a class="pull-right btn btn-xs btn-primary" href="{!! route('patientPhones.create') !!}">Add New</a></h3>
  </div>
  <div class="panel-body">
    @foreach($patient->phones as $phone)
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                {!! $phone->phoneNumber !!} {!! $phone->type->description !!}
            </div>
            <div class="col-xs-12 col-md-8">
                <a href="{!! route('patientPhones.edit', [$phone->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('patientPhones.delete', [$phone->id]) !!}" onclick="return confirm('Are you sure wants to delete this phone number?')"><i class="glyphicon glyphicon-remove"></i></a>
            </div>
        </div>
    @endforeach
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Episodes <a class="pull-right btn btn-xs btn-primary" href="{!! route('patientEpisodes.create', $patient->id) !!}">Add New</a></h3>
  </div>
  <div class="panel-body">
    @foreach($patient->episodes as $episode)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Episode: {!! $episode->episode_date !!} 
                    <a href="{!! route('patientEpisodes.edit', [$episode->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('patientEpisodes.delete', [$episode->id]) !!}" onclick="return confirm('Are you sure wants to delete this episode?')"><i class="glyphicon glyphicon-remove"></i></a>
                    <br/>
                    <h5>Bills <a class="pull-right btn btn-xs btn-primary" href="{!! route('patientBills.create','') !!}">Add New</a></h5>
                    @foreach($episode->bills as $bill)
                        Bill Date: <a href="{!! route('patientBills.edit', [$bill->id]) !!}">{!! $bill->bill_date !!}</a> <br/>
                    @endforeach
                </h4>
            </div>
        </div>
    @endforeach
  </div>
</div>
</div>
@endsection
