<!--- Firstname Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('firstName', 'Firstname:') !!}
    {!! Form::text('firstName', null, ['class' => 'form-control']) !!}
</div>

<!--- Middlename Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('middleName', 'Middlename:') !!}
    {!! Form::text('middleName', null, ['class' => 'form-control']) !!}
</div>

<!--- Lastname Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('lastName', 'Lastname:') !!}
    {!! Form::text('lastName', null, ['class' => 'form-control']) !!}
</div>

<!--- Birthdate Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('birthDate', 'Birthdate:') !!}
    {!! Form::text('birthDate', null, ['class' => 'form-control']) !!}
</div>

<!--- Gender Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::select('gender_type_id', $genders, null, ['class' => 'form-control']) !!}
</div>


<!--- Email Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
