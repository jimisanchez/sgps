<!--- Patient Id Field --->
<!-- <div class="form-group col-sm-6 col-lg-4"> 
    {!! Form::label('patient_id', 'Patient Id:') !!} -->
    {!! Form::hidden('patient_id', null, ['class' => 'form-control', 'readonly']) !!}
<!-- </div> -->

<div class="form-group col-sm-12">
    {!! $patient->firstName !!} {!! $patient->middleName !!} {!! $patient->lastName !!} {!! $patient->birthDate !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('episode_date','Episode Date') !!}
    {!! Form::text('episode_date', null, ['class' => 'form-control']) !!}
</div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>