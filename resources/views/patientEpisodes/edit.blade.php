@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientEpisode, ['route' => ['patientEpisodes.update', $patientEpisode->id], 'method' => 'patch']) !!}

        @include('patientEpisodes.fields')

    {!! Form::close() !!}
</div>
@endsection
