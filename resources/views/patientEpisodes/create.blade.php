@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientEpisode, ['route' => 'patientEpisodes.store']) !!}

        @include('patientEpisodes.fields')

    {!! Form::close() !!}
</div>
@endsection

@section('scripts')
<script>
$(document).ready( function() {
        $( "#episode_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy-mm-dd"
    });

});
</script>
@endsection