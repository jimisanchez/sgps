@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">PatientEpisodes</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('patientEpisodes.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($patientEpisodes->isEmpty())
                <div class="well text-center">No PatientEpisodes found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Patient</th>
                    <th>Episode Date</th>
                    <th>Bills</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($patientEpisodes as $patientEpisode)
                        <tr>
                            <td>{!! $patientEpisode->patient->firstName !!} {!! $patientEpisode->patient->middleName !!} {!! $patientEpisode->patient->lastName !!}</td>
                            <td>{!! $patientEpisode->episode_date !!}</td>
                            <td>
                                <a href="{!! route('patientBills.create', $patientEpisode->id) !!}" class="btn btn-xs btn-primary">Add New Bill</a>
                                <br/>
                                @foreach($patientEpisode->bills as $bill)
                                    {!! $bill->created_at !!} <a href="{!! route('patientBills.edit', $bill->id) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                @endforeach
                            </td>
                            <td>
                                <a href="{!! route('patientEpisodes.edit', [$patientEpisode->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('patientEpisodes.delete', [$patientEpisode->id]) !!}" onclick="return confirm('Are you sure wants to delete this PatientEpisode?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection