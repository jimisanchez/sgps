@extends('app')
@section('content')
		<div class="container-fluid">
                            <div class="jumbotron">
                            <div class="container">
                                    <div class="row">
                                            <span class="fa-stack fa-3x">
                                                <i class="fa fa-server fa-stack-1x"></i>
                                                <i class="fa fa-ban fa-stack-2x text-danger"></i>
                                            </span> No expensive servers
                                            <span class="fa-stack fa-3x">
                                                <i class="fa fa-database fa-stack-1x"></i>
                                                <i class="fa fa-ban fa-stack-2x text-danger"></i>
                                            </span> No bulky databases
                                        <i class="fa fa-user-md fa-3x"></i> Just connecting providers....
                                        <i class="fa fa-users fa-3x"></i> to patients...
                                        <i class="fa fa-stethoscope fa-3x"></i> ... for the best healthcare possible
                                </div>
                            </div>
                            </div>
  		</div>
@endsection