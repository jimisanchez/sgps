@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'patientBillCpts.store']) !!}

        @include('patientBillCpts.fields')

    {!! Form::close() !!}
</div>
@endsection
