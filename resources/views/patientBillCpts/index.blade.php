@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">PatientBillCpts</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('patientBillCpts.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($patientBillCpts->isEmpty())
                <div class="well text-center">No PatientBillCpts found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Patient Bill Id</th>
			<th>Cpt Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($patientBillCpts as $patientBillCpt)
                        <tr>
                            <td>{!! $patientBillCpt->patient_bill_id !!}</td>
					<td>{!! $patientBillCpt->cpt_id !!}</td>
                            <td>
                                <a href="{!! route('patientBillCpts.edit', [$patientBillCpt->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('patientBillCpts.delete', [$patientBillCpt->id]) !!}" onclick="return confirm('Are you sure wants to delete this PatientBillCpt?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection