@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientBillCpt, ['route' => ['patientBillCpts.update', $patientBillCpt->id], 'method' => 'patch']) !!}

        @include('patientBillCpts.fields')

    {!! Form::close() !!}
</div>
@endsection
