@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'patientBillIcds.store']) !!}

        @include('patientBillIcds.fields')

    {!! Form::close() !!}
</div>
@endsection
