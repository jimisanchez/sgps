@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientBillIcd, ['route' => ['patientBillIcds.update', $patientBillIcd->id], 'method' => 'patch']) !!}

        @include('patientBillIcds.fields')

    {!! Form::close() !!}
</div>
@endsection
