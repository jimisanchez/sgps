<!--- Patient Bill Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('patient_bill_id', 'Patient Bill Id:') !!}
    {!! Form::text('patient_bill_id', null, ['class' => 'form-control']) !!}
</div>

<!--- Icd Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('icd_id', 'Icd Id:') !!}
    {!! Form::select('icd_id', $icds, null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
