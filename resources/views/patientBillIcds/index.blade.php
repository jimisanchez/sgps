@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">PatientBillIcds</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('patientBillIcds.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($patientBillIcds->isEmpty())
                <div class="well text-center">No PatientBillIcds found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Patient Bill Id</th>
			<th>Icd Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($patientBillIcds as $patientBillIcd)
                        <tr>
                            <td>{!! $patientBillIcd->patient_bill_id !!}</td>
					<td>{!! $patientBillIcd->icd_id !!}</td>
                            <td>
                                <a href="{!! route('patientBillIcds.edit', [$patientBillIcd->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('patientBillIcds.delete', [$patientBillIcd->id]) !!}" onclick="return confirm('Are you sure wants to delete this PatientBillIcd?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection