@extends('app')

@section('content')
<div class="container">
    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Upcoming Visits</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('telehealthInvitations.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($visits->isEmpty())
                <div class="well text-center">No upcoming telehealth visits found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>User</th>
                    <th>Email</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($visits as $telehealthInvitation)
                        <tr>
                        <td>{!! $telehealthInvitation->user->name !!}</td>
                        <td>{!! $telehealthInvitation->email !!}</td>
                        <td>{!! $telehealthInvitation->sessionStartTime !!}</td>
                        <td>{!! $telehealthInvitation->sessionEndTime !!}</td>
                            <td>
                                <a href="{!! route('telehealthInvitations.edit', [$telehealthInvitation->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('telehealthInvitations.delete', [$telehealthInvitation->id]) !!}" onclick="return confirm('Are you sure wants to delete this scheduled telehealth appointment?')"><i class="glyphicon glyphicon-remove"></i></a>
                                <a href="{!! route('telehealth.connect', $telehealthInvitation->sessionHash) !!}"><li class="glyphicon glyphicon-play"></li></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>
@endsection