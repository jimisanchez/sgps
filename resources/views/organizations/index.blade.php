@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Organizations</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('organizations.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($organizations->isEmpty())
                <div class="well text-center">No Organizations found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Name</th>
                    <th>Users</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                    @foreach($organizations as $organization)
                        <tr>
                            <td>{!! $organization->name !!}</td>
                            <td>
                                    @foreach($organization->users as $user)
                                        {!! $user->name !!}<br/>
                                    @endforeach
                                    </td>
                            <td>
                                <a href="{!! route('organizations.edit', [$organization->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('organizations.delete', [$organization->id]) !!}" onclick="return confirm('Are you sure wants to delete this Organization?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>
@endsection