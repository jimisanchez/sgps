@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($organization, ['route' => ['organizations.update', $organization->id], 'method' => 'patch']) !!}

        @include('organizations.fields')

    {!! Form::close() !!}
</div>
@endsection
