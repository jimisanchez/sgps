@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($icd, ['route' => ['icds.update', $icd->id], 'method' => 'patch']) !!}

        @include('icds.fields')

    {!! Form::close() !!}
</div>
@endsection
