@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'icds.store']) !!}

        @include('icds.fields')

    {!! Form::close() !!}
</div>
@endsection
