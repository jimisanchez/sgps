@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Icds</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('icds.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($icds->isEmpty())
                <div class="well text-center">No Icds found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Code</th>
			<th>Description</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($icds as $icd)
                        <tr>
                            <td>{!! $icd->code !!}</td>
					<td>{!! $icd->description !!}</td>
                            <td>
                                <a href="{!! route('icds.edit', [$icd->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('icds.delete', [$icd->id]) !!}" onclick="return confirm('Are you sure wants to delete this Icd?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection