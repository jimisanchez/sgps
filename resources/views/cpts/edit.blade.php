@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($cpt, ['route' => ['cpts.update', $cpt->id], 'method' => 'patch']) !!}

        @include('cpts.fields')

    {!! Form::close() !!}
</div>
@endsection
