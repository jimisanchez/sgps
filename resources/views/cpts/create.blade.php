@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'cpts.store']) !!}

        @include('cpts.fields')

    {!! Form::close() !!}
</div>
@endsection
