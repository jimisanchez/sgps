@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cpts</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('cpts.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($cpts->isEmpty())
                <div class="well text-center">No Cpts found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Description</th>
			<th>Code</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($cpts as $cpt)
                        <tr>
                            <td>{!! $cpt->description !!}</td>
					<td>{!! $cpt->code !!}</td>
                            <td>
                                <a href="{!! route('cpts.edit', [$cpt->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('cpts.delete', [$cpt->id]) !!}" onclick="return confirm('Are you sure wants to delete this Cpt?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection