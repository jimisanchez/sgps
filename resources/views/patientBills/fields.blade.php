<!--- Patient Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('patient_id', 'Patient:') !!}
    <br/>
    @if ( !empty($patientBill->id) )
        <a href="{!! url('patients', $patientBill->patient_id) !!}">

    {!! $patientBill->patient->firstName !!} 
    {!! $patientBill->patient->middleName !!} 
    {!! $patientBill->patient->lastName !!} 
    {!! $patientBill->patient->birthDate !!} 
    {!! Form::hidden('patient_id', null, ['class' => 'form-control']) !!}
    </a>
    @endif
    @if (empty($patientBill->id) ) 
        {!! Form::select('patient_id', array('' => '') + $patients, null, ['class' => 'form-control']) !!}
    @endif
</div>

<!--- Episode Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('patient_episode_id', 'Episode:') !!}
    @if ( !empty($patientBill->id) )
        {!! Form::hidden('patient_episode_id', null, ['class' => 'form-control']) !!}
        <br/>
        <a href="{!! route('patientEpisodes.edit',$patientBill->episode->id) !!}">{!! $patientBill->episode->episode_date !!}</a>
    @endif
    @if ( empty($patientBill->id) )
        {!! Form::select('patient_episode_id', array(), null, ['class' => 'form-control']) !!}
    @endif
</div>

<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('bill_date','Bill Date:') !!}
    {!! Form::text('bill_date', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('cpts', 'Cpt Code') !!}
<div id="cptMagicSuggest"></div>
</div>

<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('icd', 'Icd code') !!}
<div id="icdMagicSuggest"></div>
</div>

<div class="form-group col-sm-6 col-lg-4">
    {!!Form::label('note','Note') !!}
    {!! Form::textarea('note',null, ['class' => 'form-control' , 'rows' => 5, 'cols' => 5]) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>

@section('scripts')
<link href="/css/magicsuggest-min.css" rel="stylesheet" />
<script src="/js/plugins/magicsuggest-master/magicsuggest-min.js"></script>
<script>
$(document).ready( function() {
        $( "#bill_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "yy-mm-dd"
    });
    $('#patient_id').on('change', function() {   
        $.ajax({ 
                url: '/patientEpisodes/getEpisodesByPatientId/' + parseInt($(this).val()), 
                success: function(result) { 
                    $('#patient_episode_id').find('option').remove();
                    $.each(result, function(key,value) {
                        $('#patient_episode_id').append($("<option></option>")
                                                                         .attr("value",value.id)
                                                                         .text(value.episode_date)); 
                    });
                } 
            });
    });
    $('#patient_id').trigger('change');
    var icdMS = $('#icdMagicSuggest').magicSuggest({ 
                                                        data: '/icd',
                                                        method: 'get',
                                                        name: 'icds[]',
                                                        minChars: 3
                                                        });
    icdMS.setSelection({!! $selectedIcds !!});
    var cptMS = $('#cptMagicSuggest').magicSuggest({ 
                                                        data: '/cpt',
                                                        method: 'get',
                                                        name: 'cpts[]',
                                                        minChars: 3
                                                        });
    cptMS.setSelection({!! $selectedCpts !!});

});
</script>

@endsection