@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientBill, ['route' => 'patientBills.store']) !!}

        @include('patientBills.fields')

    {!! Form::close() !!}
</div>
@endsection
