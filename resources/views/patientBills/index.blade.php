@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">PatientBills</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px"  href="{!! route('patientBills.create','') !!}">Add new</a>
        </div>
       {!!Form::open( array('method' => 'get') ) !!}

        <div class="row">
            <div class="form-group col-sm-6 col-lg-4">
                {!! Form::label('patient_id', 'Patient:') !!}
                {!! Form::select('patient_id', array('' => '') + $patients, $attributes['patient_id'], ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-6 col-lg-4">
                {!! Form::label('bill_date', 'Bill Date:') !!}
                {!! Form::text('bill_date', $attributes['bill_date'], ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-12">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            </div>

        </div>
        {!! Form::close() !!}
        <div class="row">
            @if($patientBills->isEmpty())
                <div class="well text-center">No PatientBills found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Patient</th>
                    <th>Provider</th>
                    <th>Bill Date</th>
                    <th>Episode</th>
                    <th>ICD</th>
                    <th>CPT</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($patientBills as $patientBill)
                        <tr>
                            <td><a href="/patients/{!! $patientBill->patient->id !!}/">{!! $patientBill->patient->firstName !!} {!! $patientBill->patient->middleName !!} {!! $patientBill->patient->lastName !!}</a></td>
                            <td>{!! $patientBill->provider->name !!}</td>
                            <td><a href="/patientBills/{!! $patientBill->id !!}/">{!! $patientBill->bill_date !!}</a></td>
                            <td><a href="/patientEpisodes/{!! $patientBill->episode->id !!}">{!! $patientBill->episode->episode_date !!}</a></td>
                            <td>
                                @foreach($patientBill->icds as $diagnosis)
                                    <a target="_blank" href="http://www.cms.gov/medicare-coverage-database/staticpages/icd9-code-range.aspx?DocType=LCD&DocID=196&Group=1&RangeStart={!! $diagnosis->code !!}&RangeEnd={!! $diagnosis->code !!}">{!! $diagnosis->code !!} {!! $diagnosis->description !!}</a><br/>
                                @endforeach
                            </td>
                            <td>
                                @foreach($patientBill->cpts as $procedure)
                                    <a target="_blank" href="https://www.cms.gov/apps/physician-fee-schedule/search/search-results.aspx?Y=0&T=0&HT=0&CT=3&H1={!! $procedure->code !!}&M=5">{!! $procedure->code !!} {!! $procedure->description !!}</a><br/>
                                @endforeach
                            </td>
                            <td>
                                <a href="{!! route('patientBills.edit', [$patientBill->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('patientBills.delete', [$patientBill->id]) !!}" onclick="return confirm('Are you sure wants to delete this PatientBill?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection