@extends('app')

@section('content')
<div class="container">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Patient</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 col-md-4">
                    Name: <a href="/patients/{!! $patientBill->patient->id !!}/">{!! $patientBill->patient->firstName !!} {!! $patientBill->patient->middleName !!} {!! $patientBill->patient->lastName !!} </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-4">
                                Birth Date: {!! $patientBill->patient->birthDate !!}
                </div>
            </div>
        </div>
        </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Diagnosis</h3>
        </div>
        <div class="panel-body">
        @foreach($patientBill->icds as $diagnosis)
            <div class="row">
                <div class="col-xs-6 col-md-4">
                    {!! $diagnosis->icd->code !!} {!! $diagnosis->icd->description !!}  
                    <a href="/patientBillIcds/{!! $diagnosis->id !!}/edit"><i class="glyphicon glyphicon-edit"></i></a>  
                    <a href="{!! route('patientBillIcds.delete', [$diagnosis->id]) !!}" onclick="return confirm('Are you sure wants to delete this diagnosis?')"><i class="glyphicon glyphicon-remove"></i></a>           
                </div>
            </div>
        @endforeach
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Procedures</h3>
        </div>
        <div class="panel-body">
        @foreach($patientBill->cpts as $procedures)
            <div class="row">
                <div class="col-xs-6 col-md-4">
                    {!! $procedures->cpt->code !!} {!! $procedures->cpt->description !!}                    
                    <a href="/patientBillCpts/{!! $procedures->id !!}/edit"><i class="glyphicon glyphicon-edit"></i></a>  
                    <a href="{!! route('patientBillCpts.delete', [$procedures->id]) !!}" onclick="return confirm('Are you sure wants to delete this procedure?')"><i class="glyphicon glyphicon-remove"></i></a>           

                </div>
            </div>
        @endforeach
        </div>
    </div>

</div>
@endsection
