@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($patientBill, ['route' => ['patientBills.update', $patientBill->id], 'method' => 'patch']) !!}

        @include('patientBills.fields')

    {!! Form::close() !!}
</div>
@endsection
