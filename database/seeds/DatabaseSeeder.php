<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use DJB\Confer\Conversation;
use App\User;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('UserTableSeeder');
		//$this->call('ConferSeeder');
		$conversation = Conversation::create([
			'name' => 'Global',
			'is_private' => false
		]);

		foreach (User::all() as $user)
		{
			$user->conversations()->attach($conversation->id);
		}

	}

}
