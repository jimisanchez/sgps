<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePatientBillIcdRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\PatientBillIcdRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PatientBillIcdController extends AppBaseController
{

	/** @var  PatientBillIcdRepository */
	private $patientBillIcdRepository;

	function __construct(PatientBillIcdRepository $patientBillIcdRepo)
	{
		$this->patientBillIcdRepository = $patientBillIcdRepo;
	}

	/**
	 * Display a listing of the PatientBillIcd.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->patientBillIcdRepository->search($input);

		$patientBillIcds = $result[0];

		$attributes = $result[1];

		return view('patientBillIcds.index')
		    ->with('patientBillIcds', $patientBillIcds)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new PatientBillIcd.
	 *
	 * @return Response
	 */
	public function create()
	{
		$icds = \App\Models\Icd::lists('description', 'id');
		return view('patientBillIcds.create')->with('icds', $icds);;
	}

	/**
	 * Store a newly created PatientBillIcd in storage.
	 *
	 * @param CreatePatientBillIcdRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePatientBillIcdRequest $request)
	{
        $input = $request->all();

		$patientBillIcd = $this->patientBillIcdRepository->store($input);

		Flash::message('PatientBillIcd saved successfully.');

		return redirect(route('patientBillIcds.index'));
	}

	/**
	 * Display the specified PatientBillIcd.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$patientBillIcd = $this->patientBillIcdRepository->findPatientBillIcdById($id);

		if(empty($patientBillIcd))
		{
			Flash::error('PatientBillIcd not found');
			return redirect(route('patientBillIcds.index'));
		}

		return view('patientBillIcds.show')->with('patientBillIcd', $patientBillIcd);
	}

	/**
	 * Show the form for editing the specified PatientBillIcd.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patientBillIcd = $this->patientBillIcdRepository->findPatientBillIcdById($id);
		$icds = \App\Models\Icd::lists('description', 'id');

		if(empty($patientBillIcd))
		{
			Flash::error('PatientBillIcd not found');
			return redirect(route('patientBillIcds.index'));
		}

		return view('patientBillIcds.edit')->with('patientBillIcd', $patientBillIcd)->with('icds', $icds);
	}

	/**
	 * Update the specified PatientBillIcd in storage.
	 *
	 * @param  int    $id
	 * @param CreatePatientBillIcdRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePatientBillIcdRequest $request)
	{
		$patientBillIcd = $this->patientBillIcdRepository->findPatientBillIcdById($id);

		if(empty($patientBillIcd))
		{
			Flash::error('PatientBillIcd not found');
			return redirect(route('patientBillIcds.index'));
		}

		$patientBillIcd = $this->patientBillIcdRepository->update($patientBillIcd, $request->all());

		Flash::message('PatientBillIcd updated successfully.');

		return redirect(route('patientBillIcds.index'));
	}

	/**
	 * Remove the specified PatientBillIcd from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$patientBillIcd = $this->patientBillIcdRepository->findPatientBillIcdById($id);

		if(empty($patientBillIcd))
		{
			Flash::error('PatientBillIcd not found');
			return redirect(route('patientBillIcds.index'));
		}

		$patientBillIcd->delete();

		Flash::message('PatientBillIcd deleted successfully.');

		return redirect(route('patientBillIcds.index'));
	}

}
