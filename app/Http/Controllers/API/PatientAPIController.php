<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use Mitul\Controller\AppBaseController;
use Mitul\Generator\Utils\ResponseManager;
use App\Models\Patient;
use Illuminate\Http\Request;
use App\Libraries\Repositories\PatientRepository;
use Response;

class PatientAPIController extends AppBaseController
{

	/** @var  PatientRepository */
	private $patientRepository;

	function __construct(PatientRepository $patientRepo)
	{
		$this->patientRepository = $patientRepo;
	}

	/**
	 * Display a listing of the Patient.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->patientRepository->search($input);

		$patients = $result[0];

		return Response::json(ResponseManager::makeResult($patients->toArray(), "Patients retrieved successfully."));
	}

	public function search($input)
        {
            $query = Patient::query();

            $columns = Schema::getColumnListing('$TABLE_NAME$');
            $attributes = array();

            foreach($columns as $attribute){
                if(isset($input[$attribute]))
                {
                    $query->where($attribute, $input[$attribute]);
                }
            };

            return $query->get();

        }

	/**
	 * Show the form for creating a new Patient.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created Patient in storage.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Patient::$rules) > 0)
            $this->validateRequest($request, Patient::$rules);

        $input = $request->all();

		$patient = $this->patientRepository->store($input);

		return Response::json(ResponseManager::makeResult($patient->toArray(), "Patient saved successfully."));
	}

	/**
	 * Display the specified Patient.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$patient = $this->patientRepository->findPatientById($id);

		if(empty($patient))
			$this->throwRecordNotFoundException("Patient not found", ERROR_CODE_RECORD_NOT_FOUND);

		return Response::json(ResponseManager::makeResult($patient->toArray(), "Patient retrieved successfully."));
	}

	/**
	 * Show the form for editing the specified Patient.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Patient in storage.
	 *
	 * @param  int    $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$patient = $this->patientRepository->findPatientById($id);

		if(empty($patient))
			$this->throwRecordNotFoundException("Patient not found", ERROR_CODE_RECORD_NOT_FOUND);

		$input = $request->all();

		$patient = $this->patientRepository->update($patient, $input);

		return Response::json(ResponseManager::makeResult($patient->toArray(), "Patient updated successfully."));
	}

	/**
	 * Remove the specified Patient from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$patient = $this->patientRepository->findPatientById($id);

		if(empty($patient))
			$this->throwRecordNotFoundException("Patient not found", ERROR_CODE_RECORD_NOT_FOUND);

		$patient->delete();

		return Response::json(ResponseManager::makeResult($id, "Patient deleted successfully."));
	}

}
