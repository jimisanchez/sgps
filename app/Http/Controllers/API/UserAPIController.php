<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use Mitul\Controller\AppBaseController;
use Mitul\Generator\Utils\ResponseManager;
use App\Models\Patient;
use Illuminate\Http\Request;
use App\Libraries\Repositories\UserRepository;
use Response;

class UserAPIController extends AppBaseController
{

    /** @var  UserRepository */
    private $userRepository;

    function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the Patient.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->all();

        $result = $this->userRepository->search($input);

        $users = $result[0];

        return Response::json(ResponseManager::makeResult($users->toArray(), "Users retrieved successfully."));
    }

    public function search($input)
        {
            $query = User::query();

            $columns = Schema::getColumnListing('users');
            $attributes = array();

            foreach($columns as $attribute){
                if(isset($input[$attribute]))
                {
                    $query->where($attribute, $input[$attribute]);
                }
            };

            return $query->get();

        }

    /**
     * Show the form for creating a new Patient.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created Patient in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if(sizeof(User::$rules) > 0)
            $this->validateRequest($request, Patient::$rules);

        $input = $request->all();

        $patient = $this->userRepository->store($input);

        return Response::json(ResponseManager::makeResult($patient->toArray(), "User saved successfully."));
    }

    /**
     * Display the specified Patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findPatientById($id);

        if(empty($user))
            $this->throwRecordNotFoundException("User not found", ERROR_CODE_RECORD_NOT_FOUND);

        return Response::json(ResponseManager::makeResult($patient->toArray(), "User retrieved successfully."));
    }

    /**
     * Show the form for editing the specified Patient.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified Patient in storage.
     *
     * @param  int    $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $user = $this->userRepository->findUserById($id);

        if(empty($user))
            $this->throwRecordNotFoundException("User not found", ERROR_CODE_RECORD_NOT_FOUND);

        $input = $request->all();

        $user = $this->userRepository->update($user, $input);

        return Response::json(ResponseManager::makeResult($user->toArray(), "User updated successfully."));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findUserById($id);

        if(empty($user))
            $this->throwRecordNotFoundException("User not found", ERROR_CODE_RECORD_NOT_FOUND);

        $user->delete();

        return Response::json(ResponseManager::makeResult($id, "User deleted successfully."));
    }

}
