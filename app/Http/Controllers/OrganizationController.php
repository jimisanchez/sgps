<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateOrganizationRequest;
use App\Libraries\Repositories\OrganizationRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class OrganizationController extends AppBaseController
{

	/** @var  OrganizationRepository */
	private $organizationRepository;

	function __construct(OrganizationRepository $organizationRepo)
	{
		$this->organizationRepository = $organizationRepo;
	}

	/**
	 * Display a listing of the Organization.
	 *
	 * @return Response
	 */
	public function index()
	{
		\DB::connection()->enableQueryLog();
		$organizations = $this->organizationRepository->all();

		return view('organizations.index')->with('organizations', $organizations);
	}

	/**
	 * Show the form for creating a new Organization.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('organizations.create');
	}

	/**
	 * Store a newly created Organization in storage.
	 *
	 * @param CreateOrganizationRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateOrganizationRequest $request)
	{
      		  $input = $request->all();

		$organization = $this->organizationRepository->store($input);

		Flash::message('Organization saved successfully.');

		return redirect(route('organizations.index'));
	}

	/**
	 * Display the specified Organization.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$organization = $this->organizationRepository->findOrganizationById($id);

		if(empty($organization))
		{
			Flash::error('Organization not found');
			return redirect(route('organizations.index'));
		}

		return view('organizations.show')->with('organization', $organization);
	}

	/**
	 * Show the form for editing the specified Organization.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$organization = $this->organizationRepository->findOrganizationById($id);

		if(empty($organization))
		{
			Flash::error('Organization not found');
			return redirect(route('organizations.index'));
		}

		return view('organizations.edit')->with('organization', $organization);
	}

	/**
	 * Update the specified Organization in storage.
	 *
	 * @param  int    $id
	 * @param CreateOrganizationRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateOrganizationRequest $request)
	{
		$organization = $this->organizationRepository->findOrganizationById($id);

		if(empty($organization))
		{
			Flash::error('Organization not found');
			return redirect(route('organizations.index'));
		}

		$organization = $this->organizationRepository->update($organization, $request->all());

		Flash::message('Organization updated successfully.');

		return redirect(route('organizations.index'));
	}

	/**
	 * Remove the specified Organization from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$organization = $this->organizationRepository->findOrganizationById($id);

		if(empty($organization))
		{
			Flash::error('Organization not found');
			return redirect(route('organizations.index'));
		}

		$organization->delete();

		Flash::message('Organization deleted successfully.');

		return redirect(route('organizations.index'));
	}

}
