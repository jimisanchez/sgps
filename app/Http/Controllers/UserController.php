<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Libraries\Repositories\UserRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class UserController extends AppBaseController
{

	/** @var  UserRepository */
	private $userRepository;

	function __construct(UserRepository $userRepo)
	{
		$this->middleware('auth');
		$this->userRepository = $userRepo;
	}

	/**
	 * Display a listing of the User.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = $this->userRepository->all();
		return view('users.index')->with('users', $users);
	}

	/**
	 * Show the form for creating a new User.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = new \App\User();
		$roles = \App\Role::all()->lists('display_name','id');
		return view('users.create')->with('roles',$roles)->with('user', $user);
	}

	/**
	 * Store a newly created User in storage.
	 *
	 * @param CreateUserRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateUserRequest $request)
	{
        		$input = $request->all();
        		$input['password'] = \Hash::make('123456');

        		$input['current_org_id'] = \Auth::user()->current_org_id;

		$user = $this->userRepository->store($input);

		$userOrganization = new \App\UserOrganization();
		$userOrganization->user_id = $user->id;
		$userOrganization->organization_id = \Auth::user()->current_org_id;
		$userOrganization->save();


		foreach($request->roles as $role) {
			$roleUser = new \App\Models\RoleUser();
			$roleUser->user_id = $user->id;
			$roleUser->role_id = $role;
			$roleUser->save();
		}

		Flash::message('User saved successfully.');

		return redirect(route('users.index'));
	}

	/**
	 * Display the specified User.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$user = $this->userRepository->findUserById($id);

		if(empty($user))
		{
			Flash::error('User not found');
			return redirect(route('users.index'));
		}

		return view('users.show')->with('user', $user);
	}

	/**
	 * Show the form for editing the specified User.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->userRepository->findUserById($id);
		$roles = \App\Role::all()->lists('display_name','id');

		if(empty($user))
		{
			Flash::error('User not found');
			return redirect(route('users.index'));
		}

		return view('users.edit')->with('user', $user)->with('roles', $roles);
	}

	/**
	 * Update the specified User in storage.
	 *
	 * @param  int    $id
	 * @param CreateUserRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateUserRequest $request)
	{
		$user = $this->userRepository->findUserById($id);

		if(empty($user))
		{
			Flash::error('User not found');
			return redirect(route('users.index'));
		}

		$user = $this->userRepository->update($user, $request->all());
		
		if ($user->id != \Auth::user()->id) {
			$user->roles()->sync( $request->get('roles') );
			$user->push();			
		}

		Flash::message('User updated successfully.');

		return redirect(route('users.index'));
	}

	/**
	 * Remove the specified User from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = $this->userRepository->findUserById($id);

		if(empty($user))
		{
			Flash::error('User not found');
			return redirect(route('users.index'));
		}

//		$user->delete();
		$orgId = \Auth::user()->current_org_id;
		$userOrg = \App\UserOrganization::where('user_id', $id)->where('organization_id', $orgId);

		$userOrg->delete();

		Flash::message('User deleted successfully.');

		return redirect(route('users.index'));
	}

}
