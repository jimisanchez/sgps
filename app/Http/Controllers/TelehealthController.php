<?php namespace App\Http\Controllers;

use OpenTok\OpenTok;
use Flash;

class TelehealthController extends Controller
{

    protected $openTok;
    protected $session;

    public function __construct()
    {
/*
        if ( \Auth::check() ) {
            $this->openTok = new OpenTok('45243742', '39138e5c47e0add08f2e15e65417a449c48791f6');
            $userTelehealthSession = \App\Models\TelehealthSession::where('user_id','=', \Auth::user()->id)->get();
            
            if ( empty($userTelehealthSession->toArray()) ) {
                
                $session = $this->openTok->createSession();
                
                $telehealthSession = new \App\Models\TelehealthSession();

                $telehealthSession->session = $session;
                $telehealthSession->user_id = \Auth::user()->id;
                $telehealthSession->token = $session->generateToken();
                $telehealthSession->save();

            }
            $userTelehealthSession = \App\Models\TelehealthSession::where('user_id','=', \Auth::user()->id)->get();
            $this->session = $userTelehealthSession->first();
        }
*/
    }

    public function index()
    {
        if (\Auth::user()->hasRole('admin') ) {
            $upcomingVisits = \App\Models\TelehealthInvitation::where('sessionStartTime','>', date('Y-m-d H:i:s', strtotime('-20 minutes')))->get();
        } else if (\Auth::user()->hasRole('provider') ) {
            $upcomingVisits = \App\Models\TelehealthInvitation::where('sessionStartTime','>', date('Y-m-d H:i:s', strtotime('-20 minutes')))->where('user_id', \Auth::user()->id)->get();            
        }
        return view('telehealth/index')->with('visits', $upcomingVisits);
    }

    public function connect($session)
    {
        $visit = \App\Models\TelehealthInvitation::where('sessionHash','=', $session)->get()->first();

        if ( empty($visit->id) ) {
            Flash::error('Unable to locate session');
            return redirect(route('telehealth.index'));
        }

        if ( strtotime($visit->sessionStartTime) > strtotime('+15 minutes') ) {
            Flash::error('You may not join a session earlier than 15 minutes before the start time');
            return redirect(route('telehealth.index') );
        }
        return view('telehealth/connect')->with('session', $session);
    }

}