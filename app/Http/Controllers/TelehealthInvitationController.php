<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTelehealthInvitationRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\TelehealthInvitationRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class TelehealthInvitationController extends AppBaseController
{

	/** @var  TelehealthInvitationRepository */
	private $telehealthInvitationRepository;

	function __construct(TelehealthInvitationRepository $telehealthInvitationRepo)
	{
		$this->telehealthInvitationRepository = $telehealthInvitationRepo;
	}

	/**
	 * Display a listing of the TelehealthInvitation.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    	$input = $request->all();

		$result = $this->telehealthInvitationRepository->search($input);

		$telehealthInvitations = $result[0];

		$attributes = $result[1];

		return view('telehealthInvitations.index')
		    ->with('telehealthInvitations', $telehealthInvitations)
		    ->with('attributes', $attributes);
	}

	/**
	 * Show the form for creating a new TelehealthInvitation.
	 *
	 * @return Response
	 */
	public function create()
	{
		$organization_id = \Auth::user()->current_org_id;
		$users = \App\User::join('user_organizations','users.id','=','user_organizations.user_id')->where('user_organizations.organization_id',$organization_id)->get(array('users.*'))->lists('name','id');
		return view('telehealthInvitations.create')->with('users', $users);
	}

	/**
	 * Store a newly created TelehealthInvitation in storage.
	 *
	 * @param CreateTelehealthInvitationRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTelehealthInvitationRequest $request)
	{
		$input = $request->all();
		$input['sessionHash'] = \Uuid::generate(1);
		$input['organization_id'] = \Auth::user()->current_org_id;

		$telehealthInvitation = $this->telehealthInvitationRepository->store($input);

		Flash::message('TelehealthInvitation saved successfully.');

		return redirect(route('telehealthInvitations.index'));
	}

	/**
	 * Display the specified TelehealthInvitation.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$telehealthInvitation = $this->telehealthInvitationRepository->findTelehealthInvitationById($id);

		if(empty($telehealthInvitation))
		{
			Flash::error('TelehealthInvitation not found');
			return redirect(route('telehealthInvitations.index'));
		}

		return view('telehealthInvitations.show')->with('telehealthInvitation', $telehealthInvitation);
	}

	/**
	 * Show the form for editing the specified TelehealthInvitation.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$telehealthInvitation = $this->telehealthInvitationRepository->findTelehealthInvitationById($id);
		$organization_id = \Auth::user()->current_org_id;
		if ( ! $this->canEditDeleteRecord($telehealthInvitation)  ) {
			Flash::error('You do not have access to this record');
			return redirect(  route('telehealthInvitations.index') );
		}
		$users = \App\User::join('user_organizations','users.id','=','user_organizations.user_id')->where('user_organizations.organization_id',$organization_id)->get(array('users.*'))->lists('name','id');

		if(empty($telehealthInvitation))
		{
			Flash::error('Telehealth invitation not found');
			return redirect(route('telehealthInvitations.index'));
		}

		return view('telehealthInvitations.edit')->with('telehealthInvitation', $telehealthInvitation)->with('users', $users);
	}

	/**
	 * Update the specified TelehealthInvitation in storage.
	 *
	 * @param  int    $id
	 * @param CreateTelehealthInvitationRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateTelehealthInvitationRequest $request)
	{
		$telehealthInvitation = $this->telehealthInvitationRepository->findTelehealthInvitationById($id);

		if(empty($telehealthInvitation))
		{
			Flash::error('TelehealthInvitation not found');
			return redirect(route('telehealthInvitations.index'));
		}

		$telehealthInvitation = $this->telehealthInvitationRepository->update($telehealthInvitation, $request->all());

		Flash::message('TelehealthInvitation updated successfully.');

		return redirect(route('telehealthInvitations.index'));
	}

	/**
	 * Remove the specified TelehealthInvitation from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$telehealthInvitation = $this->telehealthInvitationRepository->findTelehealthInvitationById($id);

		if ( !$this->canEditDeleteRecord($telehealthInvitation) )  {
			Flash::error('You do not have permission to delete this record');
			return redirect( route('telehealthInvitations.index') );
		}

		if(empty($telehealthInvitation))
		{
			Flash::error('Telehealth invitation not found');
			return redirect(route('telehealthInvitations.index'));
		}

		$telehealthInvitation->delete();

		Flash::message('TelehealthInvitation deleted successfully.');

		return redirect(route('telehealthInvitations.index'));
	}


	public function canEditDeleteRecord(\App\Models\TelehealthInvitation $invitation)
	{
		$user = \Auth::user();

		if ($user->hasRole('admin')) {
			return true;
		}

		if ( $user->hasRole('provider') && $invitation->user_id == $user->id) {
			return true;
		}

		return false;
	}
}
