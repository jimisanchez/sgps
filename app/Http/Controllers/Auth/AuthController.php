<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Aloha\Twilio\Twilio;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getRegister()
	{
		$organizations = \App\Models\Organization::take(5)->lists('name','id');
		return view('auth.register')->with('organizations', $organizations);
	}


	public function postRegister(Request $request)
	{

		$validator = $this->registrar->validator($request->all());


		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}

		$request['current_org_id'] = $request->organization[0];
		$user = $this->registrar->create($request->all());
		
		$role = \App\Role::where('name','=','admin')->get()->first();
		$userRole =  new \App\Models\RoleUser();
		$userRole->role_id = $role->id;
		$userRole->user_id = $user->id;
		$userRole->save();

		$this->auth->login($user);

		$twc = \Config::get('twilio');
		$twConfig = $twc['twilio']['connections']['twilio'];
		$hash = \Hash::make('123456');
		$twilio = new \Aloha\Twilio\Twilio($twConfig['sid'], $twConfig['token'], $twConfig['from'] );
		$twilio->message('+19105385441', 'Your registration verification code is: ' . $hash);

		return redirect($this->redirectPath());
	}
}
