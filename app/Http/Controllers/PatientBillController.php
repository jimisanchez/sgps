<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePatientBillRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\PatientBillRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PatientBillController extends AppBaseController
{

	/** @var  PatientBillRepository */
	private $patientBillRepository;

	function __construct(PatientBillRepository $patientBillRepo)
	{
		$this->patientBillRepository = $patientBillRepo;
	}

	/**
	 * Display a listing of the PatientBill.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		\DB::connection()->enableQueryLog();
		$input = $request->all();

		$result = $this->patientBillRepository->search($input);

		$patientBills = $result[0];

		$attributes = $result[1];
		$patients = \App\Models\Patient::all()->lists('full_name','id');
		return view('patientBills.index')
		    ->with('patientBills', $patientBills)
		    ->with('attributes', $attributes)
		    ->with('patients', $patients);
	}

	/**
	 * Show the form for creating a new PatientBill.
	 *
	 * @return Response
	 */
	public function create()
	{
		$patientBill = new \App\Models\PatientBill();
		$selectedCpts = json_encode(array());
		$selectedIcds = json_encode(array());
		$patients = \App\Models\Patient::all()->lists('full_name','id');

		return view('patientBills.create')->with('patients',$patients)->with('patientBill', $patientBill)->with('selectedCpts', $selectedCpts)->with('selectedIcds',$selectedIcds);
	}

	/**
	 * Store a newly created PatientBill in storage.
	 *
	 * @param CreatePatientBillRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePatientBillRequest $request)
	{
        		$input = $request->all();

		$patientBill = $this->patientBillRepository->store($input);
		$patientBill->cpts()->sync($request->get('cpts') );
		$patientBill->icds()->sync($request->get('icds'));

		Flash::message('PatientBill saved successfully.');

		return redirect(route('patientBills.index'));
	}

	/**
	 * Display the specified PatientBill.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$patientBill = $this->patientBillRepository->findPatientBillById($id);

		if(empty($patientBill))
		{
			Flash::error('PatientBill not found');
			return redirect(route('patientBills.index'));
		}

		return view('patientBills.show')->with('patientBill', $patientBill);
	}

	/**
	 * Show the form for editing the specified PatientBill.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patientBill = $this->patientBillRepository->findPatientBillById($id);
		$icds = array();
		foreach($patientBill->icds as $icd) {
			$icds[] = array('id' => $icd->id, 'name' => $icd->code . ' ' . $icd->description);
		}

		$selectedIcds = json_encode($icds);

		$cpts = array();
		foreach($patientBill->cpts as $cpt) {
			$cpts[] = array('id' => $cpt->id, 'name' => $cpt->code . ' ' . $cpt->description);
		}

		$selectedCpts = json_encode($cpts);

		if(empty($patientBill))
		{
			Flash::error('PatientBill not found');
			return redirect(route('patientBills.index'));
		}

		return view('patientBills.edit')->with('patientBill', $patientBill)->with('selectedIcds', $selectedIcds)->with('selectedCpts', $selectedCpts);
	}

	/**
	 * Update the specified PatientBill in storage.
	 *
	 * @param  int    $id
	 * @param CreatePatientBillRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePatientBillRequest $request)
	{
		$patientBill = $this->patientBillRepository->findPatientBillById($id);

		if(empty($patientBill))
		{
			Flash::error('PatientBill not found');
			return redirect(route('patientBills.index'));
		}

		$patientBill = $this->patientBillRepository->update($patientBill, $request->all());
		$patientBill->cpts()->sync($request->get('cpts') );
		$patientBill->icds()->sync($request->get('icds'));
		Flash::message('PatientBill updated successfully.');

		return redirect(route('patientBills.index'));
	}

	/**
	 * Remove the specified PatientBill from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$patientBill = $this->patientBillRepository->findPatientBillById($id);

		if(empty($patientBill))
		{
			Flash::error('PatientBill not found');
			return redirect(route('patientBills.index'));
		}

		$patientBill->delete();

		Flash::message('PatientBill deleted successfully.');

		return redirect(route('patientBills.index'));
	}

}
