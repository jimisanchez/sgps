<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePatientBillCptRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\PatientBillCptRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PatientBillCptController extends AppBaseController
{

	/** @var  PatientBillCptRepository */
	private $patientBillCptRepository;

	function __construct(PatientBillCptRepository $patientBillCptRepo)
	{
		$this->patientBillCptRepository = $patientBillCptRepo;
	}

	/**
	 * Display a listing of the PatientBillCpt.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->patientBillCptRepository->search($input);

		$patientBillCpts = $result[0];

		$attributes = $result[1];

		return view('patientBillCpts.index')
		    ->with('patientBillCpts', $patientBillCpts)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new PatientBillCpt.
	 *
	 * @return Response
	 */
	public function create()
	{
		$cpts = \App\Models\Cpt::lists('description','id');
		return view('patientBillCpts.create')->with('cpts',$cpts);
	}

	/**
	 * Store a newly created PatientBillCpt in storage.
	 *
	 * @param CreatePatientBillCptRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePatientBillCptRequest $request)
	{
        $input = $request->all();

		$patientBillCpt = $this->patientBillCptRepository->store($input);

		Flash::message('PatientBillCpt saved successfully.');

		return redirect(route('patientBillCpts.index'));
	}

	/**
	 * Display the specified PatientBillCpt.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$patientBillCpt = $this->patientBillCptRepository->findPatientBillCptById($id);

		if(empty($patientBillCpt))
		{
			Flash::error('PatientBillCpt not found');
			return redirect(route('patientBillCpts.index'));
		}

		return view('patientBillCpts.show')->with('patientBillCpt', $patientBillCpt);
	}

	/**
	 * Show the form for editing the specified PatientBillCpt.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patientBillCpt = $this->patientBillCptRepository->findPatientBillCptById($id);
		$cpts = \App\Models\Cpt::lists('description','id');

		if(empty($patientBillCpt))
		{
			Flash::error('PatientBillCpt not found');
			return redirect(route('patientBillCpts.index'));
		}

		return view('patientBillCpts.edit')->with('patientBillCpt', $patientBillCpt)->with('cpts',$cpts);
	}

	/**
	 * Update the specified PatientBillCpt in storage.
	 *
	 * @param  int    $id
	 * @param CreatePatientBillCptRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePatientBillCptRequest $request)
	{
		$patientBillCpt = $this->patientBillCptRepository->findPatientBillCptById($id);

		if(empty($patientBillCpt))
		{
			Flash::error('PatientBillCpt not found');
			return redirect(route('patientBillCpts.index'));
		}

		$patientBillCpt = $this->patientBillCptRepository->update($patientBillCpt, $request->all());

		Flash::message('PatientBillCpt updated successfully.');

		return redirect(route('patientBillCpts.index'));
	}

	/**
	 * Remove the specified PatientBillCpt from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$patientBillCpt = $this->patientBillCptRepository->findPatientBillCptById($id);

		if(empty($patientBillCpt))
		{
			Flash::error('PatientBillCpt not found');
			return redirect(route('patientBillCpts.index'));
		}

		$patientBillCpt->delete();

		Flash::message('PatientBillCpt deleted successfully.');

		return redirect(route('patientBillCpts.index'));
	}

}
