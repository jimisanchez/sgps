<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePatientRequest;
use App\Libraries\Repositories\PatientRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PatientController extends AppBaseController
{

	/** @var  PatientRepository */
	private $patientRepository;

	function __construct(PatientRepository $patientRepo)
	{
		$this->middleware('auth');
		$this->patientRepository = $patientRepo;
	}

	/**
	 * Display a listing of the Patient.
	 *
	 * @return Response
	 */
	public function index()
	{
		$patients = $this->patientRepository->all();

		return view('patients.index')->with('patients', $patients);
	}

	/**
	 * Show the form for creating a new Patient.
	 *
	 * @return Response
	 */
	public function create()
	{
		$genders = \DB::table('gender_types')->lists('description','id');
		return view('patients.create')->with('genders',$genders);
	}

	/**
	 * Store a newly created Patient in storage.
	 *
	 * @param CreatePatientRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePatientRequest $request)
	{
        $input = $request->all();

		$patient = $this->patientRepository->store($input);

		Flash::message('Patient saved successfully.');

		return redirect(route('patients.index'));
	}

	/**
	 * Display the specified Patient.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$patient = $this->patientRepository->findPatientById($id);

		if(empty($patient))
		{
			Flash::error('Patient not found');
			return redirect(route('patients.index'));
		}

		return view('patients.show')->with('patient', $patient);
	}

	/**
	 * Show the form for editing the specified Patient.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patient = $this->patientRepository->findPatientById($id);
		$genders = \DB::table('gender_types')->lists('description','id');

		if(empty($patient))
		{
			Flash::error('Patient not found');
			return redirect(route('patients.index'));
		}

		return view('patients.edit')->with('patient', $patient)->with('genders', $genders);
	}

	/**
	 * Update the specified Patient in storage.
	 *
	 * @param  int    $id
	 * @param CreatePatientRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePatientRequest $request)
	{
		$patient = $this->patientRepository->findPatientById($id);

		if(empty($patient))
		{
			Flash::error('Patient not found');
			return redirect(route('patients.index'));
		}

		$patient = $this->patientRepository->update($patient, $request->all());

		Flash::message('Patient updated successfully.');

		return redirect(route('patients.index'));
	}

	/**
	 * Remove the specified Patient from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$patient = $this->patientRepository->findPatientById($id);

		if(empty($patient))
		{
			Flash::error('Patient not found');
			return redirect(route('patients.index'));
		}

		$patient->delete();

		Flash::message('Patient deleted successfully.');

		return redirect(route('patients.index'));
	}


	public function restore($id)
	{
		$patient = $this->patientRepository->findTrashedPatientById($id);
		if ( empty($patient))
		{
			Flash::error('Patient not found');
			return redirect(route('patients.index'));
		}
		$patient->restore();
		Flash::message('Patient undeleted successfully.');
		return redirect(route('patients.index'));
	}

}