<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePatientAddressRequest;
use App\Libraries\Repositories\PatientAddressRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PatientAddressController extends AppBaseController
{

	/** @var  PatientAddressRepository */
	private $patientAddressRepository;

	function __construct(PatientAddressRepository $patientAddressRepo)
	{
		$this->middleware('auth');
		$this->patientAddressRepository = $patientAddressRepo;
	}

	/**
	 * Display a listing of the PatientAddress.
	 *
	 * @return Response
	 */
	public function index()
	{
		$patientAddresses = $this->patientAddressRepository->all();

		return view('patientAddresses.index')->with('patientAddresses', $patientAddresses);
	}

	/**
	 * Show the form for creating a new PatientAddress.
	 *
	 * @return Response
	 */
	public function create($patient_id)
	{
		$patientAddress = new \App\Models\PatientAddress;
		$patientAddress->patient_id = $patient_id;
		$states = \DB::table('states')->lists('state_name','id');
		return view('patientAddresses.create')->with('states', $states)->with('patientAddress', $patientAddress);
	}

	/**
	 * Store a newly created PatientAddress in storage.
	 *
	 * @param CreatePatientAddressRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePatientAddressRequest $request)
	{
        		$input = $request->all();

		$patientAddress = $this->patientAddressRepository->store($input);

		Flash::message('PatientAddress saved successfully.');

		return redirect(route('patientAddresses.index'));
	}

	/**
	 * Display the specified PatientAddress.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$patientAddress = $this->patientAddressRepository->findPatientAddressById($id);

		if(empty($patientAddress))
		{
			Flash::error('PatientAddress not found');
			return redirect(route('patientAddresses.index'));
		}

		return view('patientAddresses.show')->with('patientAddress', $patientAddress);
	}

	/**
	 * Show the form for editing the specified PatientAddress.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patientAddress = $this->patientAddressRepository->findPatientAddressById($id);
		$states = \DB::table('states')->lists('state_name','id');

		if(empty($patientAddress))
		{
			Flash::error('PatientAddress not found');
			return redirect(route('patientAddresses.index'));
		}

		return view('patientAddresses.edit')->with('patientAddress', $patientAddress)->with('states', $states);
	}

	/**
	 * Update the specified PatientAddress in storage.
	 *
	 * @param  int    $id
	 * @param CreatePatientAddressRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePatientAddressRequest $request)
	{
		$patientAddress = $this->patientAddressRepository->findPatientAddressById($id);

		if(empty($patientAddress))
		{
			Flash::error('PatientAddress not found');
			return redirect(route('patientAddresses.index'));
		}

		$patientAddress = $this->patientAddressRepository->update($patientAddress, $request->all());

		Flash::message('PatientAddress updated successfully.');

		return redirect(route('patientAddresses.index'));
	}

	/**
	 * Remove the specified PatientAddress from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$patientAddress = $this->patientAddressRepository->findPatientAddressById($id);

		if(empty($patientAddress))
		{
			Flash::error('PatientAddress not found');
			return redirect(route('patientAddresses.index'));
		}

		$patientAddress->delete();

		Flash::message('PatientAddress deleted successfully.');

		return redirect(route('patientAddresses.index'));
	}

}
