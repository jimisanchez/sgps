<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePatientEpisodeRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\PatientEpisodeRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PatientEpisodeController extends AppBaseController
{

	/** @var  PatientEpisodeRepository */
	private $patientEpisodeRepository;

	function __construct(PatientEpisodeRepository $patientEpisodeRepo)
	{
		$this->patientEpisodeRepository = $patientEpisodeRepo;
	}

	/**
	 * Display a listing of the PatientEpisode.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->patientEpisodeRepository->search($input);

		$patientEpisodes = $result[0];

		$attributes = $result[1];

		return view('patientEpisodes.index')
		    ->with('patientEpisodes', $patientEpisodes)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new PatientEpisode.
	 *
	 * @return Response
	 */
	public function create($patient_id)
	{
		$patientEpisode = new \App\Models\PatientEpisode();
		$patientEpisode->patient_id = $patient_id;

		$patient = \App\Models\Patient::find($patient_id);

		return view('patientEpisodes.create')->with('patientEpisode', $patientEpisode)->with('patient', $patient);
	}

	/**
	 * Store a newly created PatientEpisode in storage.
	 *
	 * @param CreatePatientEpisodeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePatientEpisodeRequest $request)
	{
        $input = $request->all();

		$patientEpisode = $this->patientEpisodeRepository->store($input);

		Flash::message('PatientEpisode saved successfully.');

		return redirect(route('patientEpisodes.index'));
	}

	/**
	 * Display the specified PatientEpisode.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$patientEpisode = $this->patientEpisodeRepository->findPatientEpisodeById($id);

		if(empty($patientEpisode))
		{
			Flash::error('PatientEpisode not found');
			return redirect(route('patientEpisodes.index'));
		}

		return view('patientEpisodes.show')->with('patientEpisode', $patientEpisode);
	}

	/**
	 * Show the form for editing the specified PatientEpisode.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patientEpisode = $this->patientEpisodeRepository->findPatientEpisodeById($id);

		if(empty($patientEpisode))
		{
			Flash::error('PatientEpisode not found');
			return redirect(route('patientEpisodes.index'));
		}
		$patient = \App\Models\Patient::find($patientEpisode->patient_id);
		return view('patientEpisodes.edit')->with('patientEpisode', $patientEpisode)->with('patient',$patient);
	}

	/**
	 * Update the specified PatientEpisode in storage.
	 *
	 * @param  int    $id
	 * @param CreatePatientEpisodeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePatientEpisodeRequest $request)
	{
		$patientEpisode = $this->patientEpisodeRepository->findPatientEpisodeById($id);

		if(empty($patientEpisode))
		{
			Flash::error('PatientEpisode not found');
			return redirect(route('patientEpisodes.index'));
		}

		$patientEpisode = $this->patientEpisodeRepository->update($patientEpisode, $request->all());

		Flash::message('PatientEpisode updated successfully.');

		return redirect(route('patientEpisodes.index'));
	}

	/**
	 * Remove the specified PatientEpisode from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$patientEpisode = $this->patientEpisodeRepository->findPatientEpisodeById($id);

		if(empty($patientEpisode))
		{
			Flash::error('PatientEpisode not found');
			return redirect(route('patientEpisodes.index'));
		}

		$patientEpisode->delete();

		Flash::message('PatientEpisode deleted successfully.');

		return redirect(route('patientEpisodes.index'));
	}


	public function getEpisodesByPatientId($id)
	{
		$patientEpisode = \App\Models\PatientEpisode::where('patient_id', $id)->get();
		return $patientEpisode;
	}
}
