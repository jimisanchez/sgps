<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePermissionRoleRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\PermissionRoleRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PermissionRoleController extends AppBaseController
{

	/** @var  PermissionRoleRepository */
	private $permissionRoleRepository;

	function __construct(PermissionRoleRepository $permissionRoleRepo)
	{
		$this->permissionRoleRepository = $permissionRoleRepo;
	}

	/**
	 * Display a listing of the PermissionRole.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	   	 $input = $request->all();

		$result = $this->permissionRoleRepository->search($input);

		$permissionRoles = $result[0];

		$attributes = $result[1];

		return view('permissionRoles.index')
		    ->with('permissionRoles', $permissionRoles)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new PermissionRole.
	 *
	 * @return Response
	 */
	public function create()
	{
		$roles = \App\Role::all()->lists('display_name','id');
		$permissions = \App\Permission::all()->lists('display_name', 'id');
		return view('permissionRoles.create')->with('roles', $roles)->with('permissions', $permissions);
	}

	/**
	 * Store a newly created PermissionRole in storage.
	 *
	 * @param CreatePermissionRoleRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePermissionRoleRequest $request)
	{
        $input = $request->all();

		$permissionRole = $this->permissionRoleRepository->store($input);

		Flash::message('PermissionRole saved successfully.');

		return redirect(route('permissionRoles.index'));
	}

	/**
	 * Display the specified PermissionRole.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$permissionRole = $this->permissionRoleRepository->findPermissionRoleById($id);

		if(empty($permissionRole))
		{
			Flash::error('PermissionRole not found');
			return redirect(route('permissionRoles.index'));
		}

		return view('permissionRoles.show')->with('permissionRole', $permissionRole);
	}

	/**
	 * Show the form for editing the specified PermissionRole.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$permissionRole = $this->permissionRoleRepository->findPermissionRoleById($id);
		$roles = \App\Role::all()->lists('display_name','id');
		$permissions = \App\Permission::all()->lists('display_name', 'id');

		if(empty($permissionRole))
		{
			Flash::error('PermissionRole not found');
			return redirect(route('permissionRoles.index'));
		}

		return view('permissionRoles.edit')->with('permissionRole', $permissionRole)->with('roles',$roles)->with('permissions', $permissions);
	}

	/**
	 * Update the specified PermissionRole in storage.
	 *
	 * @param  int    $id
	 * @param CreatePermissionRoleRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePermissionRoleRequest $request)
	{
		$permissionRole = $this->permissionRoleRepository->findPermissionRoleById($id);

		if(empty($permissionRole))
		{
			Flash::error('PermissionRole not found');
			return redirect(route('permissionRoles.index'));
		}

		$permissionRole = $this->permissionRoleRepository->update($permissionRole, $request->all());

		Flash::message('PermissionRole updated successfully.');

		return redirect(route('permissionRoles.index'));
	}

	/**
	 * Remove the specified PermissionRole from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$permissionRole = $this->permissionRoleRepository->findPermissionRoleById($id);

		if(empty($permissionRole))
		{
			Flash::error('PermissionRole not found');
			return redirect(route('permissionRoles.index'));
		}

		$permissionRole->delete();

		Flash::message('PermissionRole deleted successfully.');

		return redirect(route('permissionRoles.index'));
	}

}
