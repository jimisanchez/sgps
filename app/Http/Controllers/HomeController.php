<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/


	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$userId = \Auth::id();
		$user = \App\User::find($userId);
		return view('home')->with('organizations', $user->organizations->lists('name','id'))->with('user', $user);
	}

	public function postIndex(Request $request)
	{	
		    $this->validate($request, [
        			'organization' => 'required',	
		    ]);

		    $user = \App\User::find(\Auth::id());
		    $user->current_org_id = $request->organization;
		    $user->save();
		
			return Redirect::back()->withSuccess('Default organization changed!');
	}

}
