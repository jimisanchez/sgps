<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateRoleUserRequest;
use App\Models\RoleUser;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class RoleUserController extends AppBaseController
{

	/**
	 * Display a listing of the RoleUser.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roleUsers = RoleUser::all();
		$query = RoleUser::query();
        $columns = Schema::getColumnListing('$TABLE_NAME$');
        $attributes = array();

        foreach($columns as $attribute){
            if($request[$attribute] == true)
            {
                $query->where($attribute, $request[$attribute]);
                $attributes[$attribute] =  $request[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        $roleUsers = $query->get();

        return view('roleUsers.index')
            ->with('roleUsers', $roleUsers)
            ->with('attributes', $attributes);
	}

	/**
	 * Show the form for creating a new RoleUser.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('roleUsers.create');
	}

	/**
	 * Store a newly created RoleUser in storage.
	 *
	 * @param CreateRoleUserRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateRoleUserRequest $request)
	{
        $input = $request->all();

		$roleUser = RoleUser::create($input);

		Flash::message('RoleUser saved successfully.');

		return redirect(route('roleUsers.index'));
	}

	/**
	 * Display the specified RoleUser.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$roleUser = RoleUser::find($id);

		if(empty($roleUser))
		{
			Flash::error('RoleUser not found');
			return redirect(route('roleUsers.index'));
		}

		return view('roleUsers.show')->with('roleUser', $roleUser);
	}

	/**
	 * Show the form for editing the specified RoleUser.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$roleUser = RoleUser::find($id);

		if(empty($roleUser))
		{
			Flash::error('RoleUser not found');
			return redirect(route('roleUsers.index'));
		}

		return view('roleUsers.edit')->with('roleUser', $roleUser);
	}

	/**
	 * Update the specified RoleUser in storage.
	 *
	 * @param  int    $id
	 * @param CreateRoleUserRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateRoleUserRequest $request)
	{
		/** @var RoleUser $roleUser */
		$roleUser = RoleUser::find($id);

		if(empty($roleUser))
		{
			Flash::error('RoleUser not found');
			return redirect(route('roleUsers.index'));
		}

		$roleUser->fill($request->all());
		$roleUser->save();

		Flash::message('RoleUser updated successfully.');

		return redirect(route('roleUsers.index'));
	}

	/**
	 * Remove the specified RoleUser from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var RoleUser $roleUser */
		$roleUser = RoleUser::find($id);

		if(empty($roleUser))
		{
			Flash::error('RoleUser not found');
			return redirect(route('roleUsers.index'));
		}

		$roleUser->delete();

		Flash::message('RoleUser deleted successfully.');

		return redirect(route('roleUsers.index'));
	}
}
