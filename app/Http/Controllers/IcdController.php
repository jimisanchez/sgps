<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateIcdRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\IcdRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class IcdController extends AppBaseController
{

	/** @var  IcdRepository */
	private $icdRepository;

	function __construct(IcdRepository $icdRepo)
	{
		$this->icdRepository = $icdRepo;
	}

	/**
	 * Display a listing of the Icd.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->icdRepository->search($input);

		$icds = $result[0];

		$attributes = $result[1];

		return view('icds.index')
		    ->with('icds', $icds)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Icd.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('icds.create');
	}

	/**
	 * Store a newly created Icd in storage.
	 *
	 * @param CreateIcdRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateIcdRequest $request)
	{
        $input = $request->all();

		$icd = $this->icdRepository->store($input);

		Flash::message('Icd saved successfully.');

		return redirect(route('icds.index'));
	}

	/**
	 * Display the specified Icd.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$icd = $this->icdRepository->findIcdById($id);

		if(empty($icd))
		{
			Flash::error('Icd not found');
			return redirect(route('icds.index'));
		}

		return view('icds.show')->with('icd', $icd);
	}

	/**
	 * Show the form for editing the specified Icd.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$icd = $this->icdRepository->findIcdById($id);

		if(empty($icd))
		{
			Flash::error('Icd not found');
			return redirect(route('icds.index'));
		}

		return view('icds.edit')->with('icd', $icd);
	}

	/**
	 * Update the specified Icd in storage.
	 *
	 * @param  int    $id
	 * @param CreateIcdRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateIcdRequest $request)
	{
		$icd = $this->icdRepository->findIcdById($id);

		if(empty($icd))
		{
			Flash::error('Icd not found');
			return redirect(route('icds.index'));
		}

		$icd = $this->icdRepository->update($icd, $request->all());

		Flash::message('Icd updated successfully.');

		return redirect(route('icds.index'));
	}

	/**
	 * Remove the specified Icd from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$icd = $this->icdRepository->findIcdById($id);

		if(empty($icd))
		{
			Flash::error('Icd not found');
			return redirect(route('icds.index'));
		}

		$icd->delete();

		Flash::message('Icd deleted successfully.');

		return redirect(route('icds.index'));
	}


	public function search(Request $request)
	{
		$keyword = $request->get('query');
		if (strlen($keyword) < 3) {
						return json_encode(array());

		}
		if ( is_numeric($keyword) ) {
			$result = \App\Models\Icd::where('code','=', $keyword)->select(\DB::raw('CONCAT(code, " ", description) AS name'),'id')->get();
		} else {
			$result = \App\Models\Icd::where('description','LIKE', '%'.$keyword .'%')->select(\DB::raw('CONCAT(code, " ", description) AS name'),'id')->get();
		}
		return $result;
	}

}
