<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePatientPhonesRequest;
use App\Libraries\Repositories\PatientPhonesRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PatientPhonesController extends AppBaseController
{

	/** @var  PatientPhonesRepository */
	private $patientPhonesRepository;

	function __construct(PatientPhonesRepository $patientPhonesRepo)
	{
		$this->middleware('auth');
		$this->patientPhonesRepository = $patientPhonesRepo;
	}

	/**
	 * Display a listing of the PatientPhones.
	 *
	 * @return Response
	 */
	public function index()
	{
		$patientPhones = $this->patientPhonesRepository->all();
		return view('patientPhones.index')->with('patientPhones', $patientPhones);
	}

	/**
	 * Show the form for creating a new PatientPhones.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('patientPhones.create');
	}

	/**
	 * Store a newly created PatientPhones in storage.
	 *
	 * @param CreatePatientPhonesRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePatientPhonesRequest $request)
	{
        $input = $request->all();

		$patientPhones = $this->patientPhonesRepository->store($input);

		Flash::message('PatientPhones saved successfully.');

		return redirect(route('patientPhones.index'));
	}

	/**
	 * Display the specified PatientPhones.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$patientPhones = $this->patientPhonesRepository->findPatientPhonesById($id);

		if(empty($patientPhones))
		{
			Flash::error('PatientPhones not found');
			return redirect(route('patientPhones.index'));
		}

		return view('patientPhones.show')->with('patientPhones', $patientPhones);
	}

	/**
	 * Show the form for editing the specified PatientPhones.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patientPhones = $this->patientPhonesRepository->findPatientPhonesById($id);
		$types = \DB::table('phone_types')->lists('description','id');
		if(empty($patientPhones))
		{
			Flash::error('PatientPhones not found');
			return redirect(route('patientPhones.index'));
		}

		return view('patientPhones.edit')->with('patientPhones', $patientPhones)->with('types', $types);
	}

	/**
	 * Update the specified PatientPhones in storage.
	 *
	 * @param  int    $id
	 * @param CreatePatientPhonesRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePatientPhonesRequest $request)
	{
		$patientPhones = $this->patientPhonesRepository->findPatientPhonesById($id);

		if(empty($patientPhones))
		{
			Flash::error('PatientPhones not found');
			return redirect(route('patientPhones.index'));
		}

		$patientPhones = $this->patientPhonesRepository->update($patientPhones, $request->all());

		Flash::message('PatientPhones updated successfully.');

		return redirect(route('patientPhones.index'));
	}

	/**
	 * Remove the specified PatientPhones from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$patientPhones = $this->patientPhonesRepository->findPatientPhonesById($id);

		if(empty($patientPhones))
		{
			Flash::error('PatientPhones not found');
			return redirect(route('patientPhones.index'));
		}

		$patientPhones->delete();

		Flash::message('PatientPhones deleted successfully.');

		return redirect(route('patientPhones.index'));
	}

}
