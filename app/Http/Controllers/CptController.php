<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCptRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\CptRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class CptController extends AppBaseController
{

	/** @var  CptRepository */
	private $cptRepository;

	function __construct(CptRepository $cptRepo)
	{
		$this->cptRepository = $cptRepo;
	}

	/**
	 * Display a listing of the Cpt.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->cptRepository->search($input);

		$cpts = $result[0];

		$attributes = $result[1];

		return view('cpts.index')
		    ->with('cpts', $cpts)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cpt.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('cpts.create');
	}

	/**
	 * Store a newly created Cpt in storage.
	 *
	 * @param CreateCptRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCptRequest $request)
	{
        $input = $request->all();

		$cpt = $this->cptRepository->store($input);

		Flash::message('Cpt saved successfully.');

		return redirect(route('cpts.index'));
	}

	/**
	 * Display the specified Cpt.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$cpt = $this->cptRepository->findCptById($id);

		if(empty($cpt))
		{
			Flash::error('Cpt not found');
			return redirect(route('cpts.index'));
		}

		return view('cpts.show')->with('cpt', $cpt);
	}

	/**
	 * Show the form for editing the specified Cpt.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cpt = $this->cptRepository->findCptById($id);

		if(empty($cpt))
		{
			Flash::error('Cpt not found');
			return redirect(route('cpts.index'));
		}

		return view('cpts.edit')->with('cpt', $cpt);
	}

	/**
	 * Update the specified Cpt in storage.
	 *
	 * @param  int    $id
	 * @param CreateCptRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCptRequest $request)
	{
		$cpt = $this->cptRepository->findCptById($id);

		if(empty($cpt))
		{
			Flash::error('Cpt not found');
			return redirect(route('cpts.index'));
		}

		$cpt = $this->cptRepository->update($cpt, $request->all());

		Flash::message('Cpt updated successfully.');

		return redirect(route('cpts.index'));
	}

	/**
	 * Remove the specified Cpt from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$cpt = $this->cptRepository->findCptById($id);

		if(empty($cpt))
		{
			Flash::error('Cpt not found');
			return redirect(route('cpts.index'));
		}

		$cpt->delete();

		Flash::message('Cpt deleted successfully.');

		return redirect(route('cpts.index'));
	}

	public function search(Request $request)
	{
		$keyword = $request->get('query');

		if ( strlen($keyword) < 3 ) {
			return json_encode( array() );
		}

		if ( is_numeric($keyword) ) {
			$result = \App\Models\Cpt::where('code','=', $keyword)->select(\DB::raw('CONCAT(code, " ", description) AS name'),'id')->get();
		} else {
			$result = \App\Models\Cpt::where('description','LIKE', $keyword .'%')->select(\DB::raw('CONCAT(code, " ", description) AS name'),'id')->get();
		}
		return $result;
	}
}
