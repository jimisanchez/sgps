<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateOrganizationPatientsRequest;
use App\Libraries\Repositories\OrganizationPatientsRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class OrganizationPatientsController extends AppBaseController
{

	/** @var  OrganizationPatientsRepository */
	private $organizationPatientsRepository;

	function __construct(OrganizationPatientsRepository $organizationPatientsRepo)
	{
		$this->organizationPatientsRepository = $organizationPatientsRepo;
	}

	/**
	 * Display a listing of the OrganizationPatients.
	 *
	 * @return Response
	 */
	public function index()
	{
		$organizationPatients = $this->organizationPatientsRepository->all();

		return view('organizationPatients.index')->with('organizationPatients', $organizationPatients);
	}

	/**
	 * Show the form for creating a new OrganizationPatients.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('organizationPatients.create');
	}

	/**
	 * Store a newly created OrganizationPatients in storage.
	 *
	 * @param CreateOrganizationPatientsRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateOrganizationPatientsRequest $request)
	{
        $input = $request->all();

		$organizationPatients = $this->organizationPatientsRepository->store($input);

		Flash::message('OrganizationPatients saved successfully.');

		return redirect(route('organizationPatients.index'));
	}

	/**
	 * Display the specified OrganizationPatients.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$organizationPatients = $this->organizationPatientsRepository->findOrganizationPatientsById($id);

		if(empty($organizationPatients))
		{
			Flash::error('OrganizationPatients not found');
			return redirect(route('organizationPatients.index'));
		}

		return view('organizationPatients.show')->with('organizationPatients', $organizationPatients);
	}

	/**
	 * Show the form for editing the specified OrganizationPatients.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$organizationPatients = $this->organizationPatientsRepository->findOrganizationPatientsById($id);

		if(empty($organizationPatients))
		{
			Flash::error('OrganizationPatients not found');
			return redirect(route('organizationPatients.index'));
		}

		return view('organizationPatients.edit')->with('organizationPatients', $organizationPatients);
	}

	/**
	 * Update the specified OrganizationPatients in storage.
	 *
	 * @param  int    $id
	 * @param CreateOrganizationPatientsRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateOrganizationPatientsRequest $request)
	{
		$organizationPatients = $this->organizationPatientsRepository->findOrganizationPatientsById($id);

		if(empty($organizationPatients))
		{
			Flash::error('OrganizationPatients not found');
			return redirect(route('organizationPatients.index'));
		}

		$organizationPatients = $this->organizationPatientsRepository->update($organizationPatients, $request->all());

		Flash::message('OrganizationPatients updated successfully.');

		return redirect(route('organizationPatients.index'));
	}

	/**
	 * Remove the specified OrganizationPatients from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$organizationPatients = $this->organizationPatientsRepository->findOrganizationPatientsById($id);

		if(empty($organizationPatients))
		{
			Flash::error('OrganizationPatients not found');
			return redirect(route('organizationPatients.index'));
		}

		$organizationPatients->delete();

		Flash::message('OrganizationPatients deleted successfully.');

		return redirect(route('organizationPatients.index'));
	}

}
