<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

#Route::get('home', 'HomeController@index');
Route::controller('home', 'HomeController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);


Route::get('authenticateapi', 'Auth\AuthenticateAPIController@authenticate');

Route::resource('api/organizations', 'API\OrganizationAPIController');

Route::resource('organizations', 'OrganizationController');

Route::get('organizations/{id}/delete', [
    'as' => 'organizations.delete',
    'uses' => 'OrganizationController@destroy',
]);


Route::resource('api/organizations', 'API\OrganizationAPIController');

Route::resource('organizations', 'OrganizationController');

Route::get('organizations/{id}/delete', [
    'as' => 'organizations.delete',
    'uses' => 'OrganizationController@destroy',
]);


Route::resource('organizations', 'OrganizationController');

Route::get('organizations/{id}/delete', [
    'as' => 'organizations.delete',
    'uses' => 'OrganizationController@destroy',
]);


Route::resource('organizations', 'OrganizationController');

Route::get('organizations/{id}/delete', [
    'as' => 'organizations.delete',
    'uses' => 'OrganizationController@destroy',
]);


Route::resource('api/organizations', 'API\OrganizationAPIController');

Route::resource('organizations', 'OrganizationController');

Route::get('organizations/{id}/delete', [
    'as' => 'organizations.delete',
    'uses' => 'OrganizationController@destroy',
]);



Route::resource('organizations', 'OrganizationController');

Route::get('organizations/{id}/delete', [
    'as' => 'organizations.delete',
    'uses' => 'OrganizationController@destroy',
]);


Route::resource('organizations', 'OrganizationController');

Route::get('organizations/{id}/delete', [
    'as' => 'organizations.delete',
    'uses' => 'OrganizationController@destroy',
]);


Route::resource('api/organizations', 'API\OrganizationAPIController');

Route::resource('organizations', 'OrganizationController');

Route::get('organizations/{id}/delete', [
    'as' => 'organizations.delete',
    'uses' => 'OrganizationController@destroy',
]);


Entrust::routeNeedsPermission('users', 'view-user', Redirect::to('/home') );
Entrust::routeNeedsPermission('users/*/edit', 'edit-user', Redirect::to('/home') );
Entrust::routeNeedsPermission('users/create', 'create-user', Redirect::to('/home') );
Route::resource('users', 'UserController');

Route::get('users/{id}/delete', [
    'as' => 'users.delete',
    'uses' => 'UserController@destroy',
]);


Route::resource('users', 'UserController');

Route::get('users/{id}/delete', [
    'as' => 'users.delete',
    'uses' => 'UserController@destroy',
]);


Route::resource('patients', 'PatientController');

// only users with roles that have the view-patient permission can view patients
Entrust::routeNeedsPermission('patients', 'view-patient', Redirect::to('/home'));
// only users with roles that have the 'create-patient' permission will be able to access the patient creation methods
Entrust::routeNeedsPermission('patients/create', 'create-patient', Redirect::to('/home'));

Route::get('patients/{id}/delete', [
    'as' => 'patients.delete',
    'uses' => 'PatientController@destroy',
]);


Route::get('patients/{id}/restore', [
    'as' => 'patients.restore',
    'uses' => 'PatientController@restore',
]);



Route::resource('patientAddresses', 'PatientAddressController');


Route::get('patientAddresses/create/{id}', [
    'as' => 'patientAddresses.create',
    'uses' => 'PatientAddressController@create'
    ]);


Route::get('patientAddresses/{id}/delete', [
    'as' => 'patientAddresses.delete',
    'uses' => 'PatientAddressController@destroy',
]);


Route::resource('patientPhones', 'PatientPhonesController');

Route::get('patientPhones/{id}/delete', [
    'as' => 'patientPhones.delete',
    'uses' => 'PatientPhonesController@destroy',
]);


Route::resource('organizationPatients', 'OrganizationPatientsController');

Route::get('organizationPatients/{id}/delete', [
    'as' => 'organizationPatients.delete',
    'uses' => 'OrganizationPatientsController@destroy',
]);


Route::resource('patientEpisodes', 'PatientEpisodeController');
Route::get('patientEpisodes/create/{id}', [
    'as' => 'patientEpisodes.create',
    'uses' => 'PatientEpisodeController@create'
    ]);
Route::get('patientEpisodes/{id}/delete', [
    'as' => 'patientEpisodes.delete',
    'uses' => 'PatientEpisodeController@destroy',
]);

Route::get('patientEpisodes/getEpisodesByPatientId/{id}', ['as' => 'patientEpisodes.getEpisodesByPatientId', 'uses' => 'PatientEpisodeController@getEpisodesByPatientId']);

Entrust::routeNeedsRole('patientBillCpts*', array('admin','provider'), Redirect::to('/home'), false);
Route::resource('patientBillCpts', 'PatientBillCptController');

Route::get('patientBillCpts/{id}/delete', [
    'as' => 'patientBillCpts.delete',
    'uses' => 'PatientBillCptController@destroy',
]);

Entrust::routeNeedsRole('patientBillIcds*', array('admin','provider'), Redirect::to('/home'), false);

Route::resource('patientBillIcds', 'PatientBillIcdController');

Route::get('patientBillIcds/{id}/delete', [
    'as' => 'patientBillIcds.delete',
    'uses' => 'PatientBillIcdController@destroy',
]);


Route::resource('cpts', 'CptController');

Route::get('cpts/{id}/delete', [
    'as' => 'cpts.delete',
    'uses' => 'CptController@destroy',
]);


Route::resource('cpts', 'CptController');

Route::get('cpts/{id}/delete', [
    'as' => 'cpts.delete',
    'uses' => 'CptController@destroy',
]);


Route::resource('cpts', 'CptController');

Route::get('cpts/{id}/delete', [
    'as' => 'cpts.delete',
    'uses' => 'CptController@destroy',
]);


Route::resource('icds', 'IcdController');

Route::get('icds/{id}/delete', [
    'as' => 'icds.delete',
    'uses' => 'IcdController@destroy',
]);

Entrust::routeNeedsRole('patientBills*',array('admin','provider'), Redirect::to('/home'), false);
Route::resource('patientBills', 'PatientBillController');
Route::get('patientBills/{id}/delete', [
    'as' => 'patientBills.delete',
    'uses' => 'PatientBillController@destroy',
]);

Route::get('patientBills/create/{id}', [
    'as' => 'patientBills.create',
    'uses' => 'PatientBillController@create',
]);


Route::resource('roleUsers', 'RoleUserController');

Route::get('roleUsers/{id}/delete', [
    'as' => 'roleUsers.delete',
    'uses' => 'RoleUserController@destroy',
]);


Route::resource('telehealth','TelehealthController');
Entrust::routeNeedsRole('telehealth', array('admin','provider'), Redirect::to('/home'), false);

Route::get('telehealth/connect/{id}', ['as' => 'telehealth.connect', 'uses' => 'TelehealthController@connect'] );

Route::resource('telehealthInvitations', 'TelehealthInvitationController');
Entrust::routeNeedsRole('telehealthInvitations/*', array('admin','provider'), Redirect::to('/home'), false );

Route::get('telehealthInvitations/{id}/delete', [
    'as' => 'telehealthInvitations.delete',
    'uses' => 'TelehealthInvitationController@destroy',
]);


Route::resource('telehealthInvitations', 'TelehealthInvitationController');

Route::get('telehealthInvitations/{id}/delete', [
    'as' => 'telehealthInvitations.delete',
    'uses' => 'TelehealthInvitationController@destroy',
]);


Route::resource('roles', 'RoleController');
Entrust::routeNeedsRole('roles*', 'super_admin', Redirect::to('/home') );
Route::get('roles/{id}/delete', [
    'as' => 'roles.delete',
    'uses' => 'RoleController@destroy',
]);


Route::resource('permissions', 'PermissionController');
Entrust::routeNeedsRole('permissions*', 'super_admin', Redirect::to('/home') );

Route::get('permissions/{id}/delete', [
    'as' => 'permissions.delete',
    'uses' => 'PermissionController@destroy',
]);


Route::resource('permissionRoles', 'PermissionRoleController');
Entrust::routeNeedsRole('permissionRoles*', 'super_admin', Redirect::to('/home') );

Route::get('permissionRoles/{id}/delete', [
    'as' => 'permissionRoles.delete',
    'uses' => 'PermissionRoleController@destroy',
]);


Route::get('icd', 'IcdController@search');
Route::get('cpt', 'CptController@search');



Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('api/patients', 'API\PatientAPIController');
    Route::resource('api/users', 'API\UserAPIController');
});