<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneType extends Model
{
    
    public $table = "phone_types";

    public $primaryKey = "id";
    
    public $timestamps = true;

    public $fillable = [
        "description"
    ];

    public static $rules = [
        "description" => "required"
    ];

}
