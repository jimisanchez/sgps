<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientBillCpt extends Model
{
    
	public $table = "patient_bill_cpts";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "patient_bill_id",
		"cpt_id"
	];

	public static $rules = [
	    "patient_bill_id" => "required",
		"cpt_id" => "required"
	];

	public function cpt()
	{
		return $this->belongsTo('App\Models\Cpt');
	}
}
