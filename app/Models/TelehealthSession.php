<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TelehealthSession extends Model
{
    
    public $table = "telehealth_sessions";

    public $primaryKey = "id";
    
    public $timestamps = false;

}
