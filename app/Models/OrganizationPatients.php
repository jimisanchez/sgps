<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizationPatients extends Model
{
    
	public $table = "organization_patients";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "patient_id",
		"organization_id"
	];

	public static $rules = [
	    "patient_id" => "required",
		"organization_id" => "required"
	];

	public function patient()
	{
		return $this->belongsTo('\App\Models\Patient');
	}


	public function organization()
	{
		return $this->belongsTo('\App\Models\Organization');
	}
}
