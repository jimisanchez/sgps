<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientBillIcd extends Model
{
    
	public $table = "patient_bill_icds";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "patient_bill_id",
		"icd_id"
	];

	public static $rules = [
	    "patient_bill_id" => "required",
		"icd_id" => "required"
	];

	public function icd()
	{
		return $this->belongsTo('App\Models\Icd');
	}

}
