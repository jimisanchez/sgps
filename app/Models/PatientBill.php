<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PatientBill extends Model
{
    
	use SoftDeletes;    
	protected $softDelete = true;

	public $table = "patient_bills";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "patient_id",
	    "patient_episode_id",
	    "bill_date",
	    "user_id",
	    "note",
	];

	public static $rules = [
	    "patient_id" => "required",
	    "patient_episode_id" => "required",
	    "bill_date" => "required"
	];

	public function cpts()
	{
		return $this->belongsToMany('App\Models\Cpt','patient_bill_cpts')->withTimestamps();
	}

	public function icds()
	{
		return $this->belongsToMany('App\Models\Icd','patient_bill_icds')->withTimestamps();
	}

	public function patient()
	{
		return $this->belongsTo('App\Models\Patient');
	}

	public function episode()
	{
		return $this->belongsTo('App\Models\PatientEpisode','patient_episode_id','id');
	}

	public function provider()
	{
		return $this->belongsTo('App\User','user_id','id');
	}
}
