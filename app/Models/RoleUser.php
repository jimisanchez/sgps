<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    
	public $table = "role_user";

	public $primaryKey = "id";
    
	public $timestamps = false;

	public $fillable = [
	    "user_id",
		"role_id"
	];

	public static $rules = [
	    "user_id" => "required",
		"role_id" => "required"
	];

}
