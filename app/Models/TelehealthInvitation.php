<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TelehealthInvitation extends Model
{
    
	public $table = "telehealth_invitations";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "email",
		"sessionHash",
		"sessionStartTime",
		"sessionEndTime",
		"user_id",
		"organization_id",
	];

	public static $rules = [
			"email" => "required|email",
			"sessionStartTime" => "required",
			"sessionEndTime" => "required",
			"user_id" => "required",
			//"organization_id" => "required"
	];

	public function user()
	{
		return $this->belongsTo('\App\User');
	}


}
