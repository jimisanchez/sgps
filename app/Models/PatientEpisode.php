<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientEpisode extends Model
{
    
	public $table = "patient_episodes";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "patient_id",
                "episode_date",
	];

	public static $rules = [
	    "patient_id" => "required",
                "episode_date" => "required",
	];

            public function bills()
            {
                return $this->hasMany('App\Models\PatientBill');
            }

            public function patient()
            {
                return $this->belongsTo('App\Models\Patient');
            }
}
