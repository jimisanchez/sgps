<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cpt extends Model
{
    
	public $table = "cpts";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "description",
		"code"
	];

	public static $rules = [
	    "description" => "required",
		"code" => "required"
	];

	public function getCptNameAttribute()
	{
		return $this->code . ' ' . $this->description;
	}

}
