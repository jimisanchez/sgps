<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PatientAddress extends Model
{

	use SoftDeletes;    
	protected $softDelete = true;
	
	public $table = "patient_addresses";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "patient_id",
		"street",
		"secondary",
		"postal",
		"state_id",
		"city"
	];

	public static $rules = [
	    "patient_id" => "required",
		"street" => "required",
		"postal" => "required",
		"state_id" => "required",
		"city" => "required"
	];

	public function patient()
	{
		return $this->belongsTo('App\Models\Patient');
	}

	public function state()
	{
		return $this->belongsTo('App\Models\State');
	}

}
