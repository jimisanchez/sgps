<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    
	public $table = "organizations";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "name"
	];

	public static $rules = [
	    "name" => "required"
	];

            public function users()
            {
                return $this->belongsToMany('App\User', 'user_organizations');
            }
}
