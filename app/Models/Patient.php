<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
	 use SoftDeletes;
    
	public $table = "patients";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "firstName",
		"middleName",
		"lastName",
		"birthDate",
		"gender_type_id",
		"email",
	];

	public static $rules = [
	    "firstName" => "required",
	    "lastName" => "required",
	    "birthDate" => "required",
	    "gender_type_id" => "required",
	    "email" => "email",
	];

	protected $softDelete = true;


	public function addresses()
	{
		return $this->hasMany('App\Models\PatientAddress');
	}

	public function phones()
	{
		return $this->hasMany('App\Models\PatientPhones');
	}

	public function gender()
	{
		return $this->belongsTo('App\Models\GenderType','gender_type_id','id');
	}

	public function organizations()
	{
		return $this->belongsTo('App\Models\OrganizationPatients');
	}

	public function episodes()
	{
		return $this->hasMany('App\Models\PatientEpisode');
	}

	public function getFullNameAttribute()
	{
		$data = array($this->firstName,$this->middleName,$this->lastName,$this->birthDate);
		return implode(' ' , $data);
	}
}