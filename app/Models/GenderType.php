<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenderType extends Model
{
    
    public $table = "gender_types";

    public $primaryKey = "id";
    
    public $timestamps = true;

}
