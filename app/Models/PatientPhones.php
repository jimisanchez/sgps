<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientPhones extends Model
{
    
	public $table = "patient_phones";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "patient_id",
		"phoneNumber",
		"type_id",
	];

	public static $rules = [
	    "patient_id" => "required",
		"phoneNumber" => "required",
		"type_id" => "required"
	];

	public function type()
	{
		return $this->belongsTo('App\Models\PhoneType');
	}

	public function patient()
	{
		return $this->belongsTo('App\Models\Patient');
	}
}
