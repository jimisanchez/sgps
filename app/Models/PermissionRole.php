<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    
	public $table = "permission_role";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "permission_id",
		"role_id"
	];

	public static $rules = [
	    "permission_id" => "required",
		"role_id" => "required"
	];

	public function permission()
	{
		return $this->belongsTo('\App\Permission');
	}

	public function role()
	{
		return $this->belongsTo('\App\Role');
	}
}
