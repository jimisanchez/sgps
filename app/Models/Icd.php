<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Icd extends Model
{
    
	public $table = "icds";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "code",
		"description"
	];

	public static $rules = [
	    "code" => "required",
		"description" => "required"
	];

	public function getIcdNameAttribute()
	{
		return $this->code . ' ' . $this->description;
	}
}
