<?php

namespace App\Libraries\Repositories;


use App\Models\Organization;

class OrganizationRepository
{

	/**
	 * Returns all Organizations
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Organization::orderBy('name','desc')->take(3)->get();
	}

	/**
	 * Stores Organization into database
	 *
	 * @param array $input
	 *
	 * @return Organization
	 */
	public function store($input)
	{
		return Organization::create($input);
	}

	/**
	 * Find Organization by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Organization
	 */
	public function findOrganizationById($id)
	{
		return Organization::find($id);
	}

	/**
	 * Updates Organization into database
	 *
	 * @param Organization $organization
	 * @param array $input
	 *
	 * @return Organization
	 */
	public function update($organization, $input)
	{
		$organization->fill($input);
		$organization->save();

		return $organization;
	}
}