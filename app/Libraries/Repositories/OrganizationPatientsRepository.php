<?php

namespace App\Libraries\Repositories;


use App\Models\OrganizationPatients;

class OrganizationPatientsRepository
{

	/**
	 * Returns all OrganizationPatients
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return OrganizationPatients::all();
	}

	/**
	 * Stores OrganizationPatients into database
	 *
	 * @param array $input
	 *
	 * @return OrganizationPatients
	 */
	public function store($input)
	{
		return OrganizationPatients::create($input);
	}

	/**
	 * Find OrganizationPatients by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|OrganizationPatients
	 */
	public function findOrganizationPatientsById($id)
	{
		return OrganizationPatients::find($id);
	}

	/**
	 * Updates OrganizationPatients into database
	 *
	 * @param OrganizationPatients $organizationPatients
	 * @param array $input
	 *
	 * @return OrganizationPatients
	 */
	public function update($organizationPatients, $input)
	{
		$organizationPatients->fill($input);
		$organizationPatients->save();

		return $organizationPatients;
	}
}