<?php

namespace App\Libraries\Repositories;


use App\Models\Icd;
use Illuminate\Support\Facades\Schema;

class IcdRepository
{

	/**
	 * Returns all Icds
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Icd::all();
	}

	public function search($input)
    {
        $query = Icd::query();

        $columns = Schema::getColumnListing('icds');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Icd into database
	 *
	 * @param array $input
	 *
	 * @return Icd
	 */
	public function store($input)
	{
		return Icd::create($input);
	}

	/**
	 * Find Icd by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Icd
	 */
	public function findIcdById($id)
	{
		return Icd::find($id);
	}

	/**
	 * Updates Icd into database
	 *
	 * @param Icd $icd
	 * @param array $input
	 *
	 * @return Icd
	 */
	public function update($icd, $input)
	{
		$icd->fill($input);
		$icd->save();

		return $icd;
	}
}