<?php

namespace App\Libraries\Repositories;


use App\Models\PatientEpisode;
use Illuminate\Support\Facades\Schema;

class PatientEpisodeRepository
{

	/**
	 * Returns all PatientEpisodes
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return PatientEpisode::all();
	}

	public function search($input)
    {
        $query = PatientEpisode::query();

        $columns = Schema::getColumnListing('patient_episodes');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores PatientEpisode into database
	 *
	 * @param array $input
	 *
	 * @return PatientEpisode
	 */
	public function store($input)
	{
		return PatientEpisode::create($input);
	}

	/**
	 * Find PatientEpisode by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|PatientEpisode
	 */
	public function findPatientEpisodeById($id)
	{
		return PatientEpisode::find($id);
	}

	/**
	 * Updates PatientEpisode into database
	 *
	 * @param PatientEpisode $patientEpisode
	 * @param array $input
	 *
	 * @return PatientEpisode
	 */
	public function update($patientEpisode, $input)
	{
		$patientEpisode->fill($input);
		$patientEpisode->save();

		return $patientEpisode;
	}
}