<?php

namespace App\Libraries\Repositories;


class UserRepository
{

	/**
	 * Returns all Users
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return $this->findUserByOrganizationId()->get(array('users.*'));
	}

	/**
	 * Stores User into database
	 *
	 * @param array $input
	 *
	 * @return User
	 */
	public function store($input)
	{
		return \App\User::create($input);
	}

	/**
	 * Find User by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|User
	 */
	public function findUserById($id)
	{
		return \App\User::find($id);
	}

	/**
	 * Updates User into database
	 *
	 * @param User $user
	 * @param array $input
	 *
	 * @return User
	 */
	public function update($user, $input)
	{
		$user->fill($input);
		$user->save();

		return $user;
	}

	public function findUserByOrganizationId()
	{
		$organization_id = \Auth::user()->current_org_id;
		$users = \App\User::join('user_organizations','users.id','=','user_organizations.user_id')->where('user_organizations.organization_id',$organization_id);
		return $users;
	}


	public function search($input)
	{
		$query = \App\User::query();

		$columns = \Schema::getColumnListing('users');
		$attributes = array();

		foreach($columns as $attribute){
			if(!empty($input[$attribute]))
			{
			$query->where($attribute, $input[$attribute]);
			$attributes[$attribute] =  $input[$attribute];
			}else{
			$attributes[$attribute] =  null;
			}
		};

		return [$query->paginate(15), $attributes];

	}
}