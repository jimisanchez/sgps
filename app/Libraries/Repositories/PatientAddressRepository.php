<?php

namespace App\Libraries\Repositories;


use App\Models\PatientAddress;

class PatientAddressRepository
{

	/**
	 * Returns all PatientAddresses
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return PatientAddress::all();
	}

	/**
	 * Stores PatientAddress into database
	 *
	 * @param array $input
	 *
	 * @return PatientAddress
	 */
	public function store($input)
	{
		return PatientAddress::create($input);
	}

	/**
	 * Find PatientAddress by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|PatientAddress
	 */
	public function findPatientAddressById($id)
	{
		return PatientAddress::find($id);
	}

	/**
	 * Updates PatientAddress into database
	 *
	 * @param PatientAddress $patientAddress
	 * @param array $input
	 *
	 * @return PatientAddress
	 */
	public function update($patientAddress, $input)
	{
		$patientAddress->fill($input);
		$patientAddress->save();

		return $patientAddress;
	}
}