<?php

namespace App\Libraries\Repositories;


use App\Models\TelehealthInvitation;
use Illuminate\Support\Facades\Schema;

class TelehealthInvitationRepository
{

	/**
	 * Returns all TelehealthInvitations
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return TelehealthInvitation::all();
	}

	public function search($input)
    {
        $query = TelehealthInvitation::query();

        $columns = Schema::getColumnListing('telehealth_invitations');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores TelehealthInvitation into database
	 *
	 * @param array $input
	 *
	 * @return TelehealthInvitation
	 */
	public function store($input)
	{
		return TelehealthInvitation::create($input);
	}

	/**
	 * Find TelehealthInvitation by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|TelehealthInvitation
	 */
	public function findTelehealthInvitationById($id)
	{
		return TelehealthInvitation::find($id);
	}

	/**
	 * Updates TelehealthInvitation into database
	 *
	 * @param TelehealthInvitation $telehealthInvitation
	 * @param array $input
	 *
	 * @return TelehealthInvitation
	 */
	public function update($telehealthInvitation, $input)
	{
		$telehealthInvitation->fill($input);
		$telehealthInvitation->save();

		return $telehealthInvitation;
	}
}