<?php

namespace App\Libraries\Repositories;


use App\Models\PatientBillIcd;
use Illuminate\Support\Facades\Schema;

class PatientBillIcdRepository
{

	/**
	 * Returns all PatientBillIcds
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return PatientBillIcd::all();
	}

	public function search($input)
    {
        $query = PatientBillIcd::query();

        $columns = Schema::getColumnListing('patient_bill_icds');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores PatientBillIcd into database
	 *
	 * @param array $input
	 *
	 * @return PatientBillIcd
	 */
	public function store($input)
	{
		return PatientBillIcd::create($input);
	}

	/**
	 * Find PatientBillIcd by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|PatientBillIcd
	 */
	public function findPatientBillIcdById($id)
	{
		return PatientBillIcd::find($id);
	}

	/**
	 * Updates PatientBillIcd into database
	 *
	 * @param PatientBillIcd $patientBillIcd
	 * @param array $input
	 *
	 * @return PatientBillIcd
	 */
	public function update($patientBillIcd, $input)
	{
		$patientBillIcd->fill($input);
		$patientBillIcd->save();

		return $patientBillIcd;
	}
}