<?php

namespace App\Libraries\Repositories;


use App\Models\PermissionRole;
use Illuminate\Support\Facades\Schema;

class PermissionRoleRepository
{

	/**
	 * Returns all PermissionRoles
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return PermissionRole::all();
	}

	public function search($input)
    {
        $query = PermissionRole::query();

        $columns = Schema::getColumnListing('permission_roles');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->orderBy('role_id','asc')->orderBy('permission_id','asc')->get(), $attributes];

    }

	/**
	 * Stores PermissionRole into database
	 *
	 * @param array $input
	 *
	 * @return PermissionRole
	 */
	public function store($input)
	{
		return PermissionRole::create($input);
	}

	/**
	 * Find PermissionRole by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|PermissionRole
	 */
	public function findPermissionRoleById($id)
	{
		return PermissionRole::find($id);
	}

	/**
	 * Updates PermissionRole into database
	 *
	 * @param PermissionRole $permissionRole
	 * @param array $input
	 *
	 * @return PermissionRole
	 */
	public function update($permissionRole, $input)
	{
		$permissionRole->fill($input);
		$permissionRole->save();

		return $permissionRole;
	}
}