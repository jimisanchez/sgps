<?php

namespace App\Libraries\Repositories;


use App\Models\PatientBill;
use Illuminate\Support\Facades\Schema;

class PatientBillRepository
{

	/**
	 * Returns all PatientBills
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return PatientBill::all();
	}

	public function search($input)
    {
        $query = PatientBill::query();

        $columns = Schema::getColumnListing('patient_bills');
        $attributes = array();

        foreach($columns as $attribute){
            if(!empty($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores PatientBill into database
	 *
	 * @param array $input
	 *
	 * @return PatientBill
	 */
	public function store($input)
	{
                        $input['user_id'] = \Auth::user()->id;
		return PatientBill::create($input);
	}

	/**
	 * Find PatientBill by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|PatientBill
	 */
	public function findPatientBillById($id)
	{
		return PatientBill::find($id);
	}

	/**
	 * Updates PatientBill into database
	 *
	 * @param PatientBill $patientBill
	 * @param array $input
	 *
	 * @return PatientBill
	 */
	public function update($patientBill, $input)
	{
		$patientBill->fill($input);
		$patientBill->save();

		return $patientBill;
	}
}