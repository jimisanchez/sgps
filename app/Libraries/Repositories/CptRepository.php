<?php

namespace App\Libraries\Repositories;


use App\Models\Cpt;
use Illuminate\Support\Facades\Schema;

class CptRepository
{

	/**
	 * Returns all Cpts
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cpt::all();
	}

	public function search($input)
    {
        $query = Cpt::query();

        $columns = Schema::getColumnListing('cpts');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cpt into database
	 *
	 * @param array $input
	 *
	 * @return Cpt
	 */
	public function store($input)
	{
		return Cpt::create($input);
	}

	/**
	 * Find Cpt by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cpt
	 */
	public function findCptById($id)
	{
		return Cpt::find($id);
	}

	/**
	 * Updates Cpt into database
	 *
	 * @param Cpt $cpt
	 * @param array $input
	 *
	 * @return Cpt
	 */
	public function update($cpt, $input)
	{
		$cpt->fill($input);
		$cpt->save();

		return $cpt;
	}
}