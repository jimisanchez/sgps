<?php

namespace App\Libraries\Repositories;


use App\Models\Patient;
use Illuminate\Support\Facades\Schema;

class PatientRepository
{

    /**
     * Returns all Patients
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return Patient::join('organization_patients','patients.id','=','organization_patients.patient_id')->where('organization_id', \Auth::user()->current_org_id)->groupby('patients.id')->withTrashed()->paginate(15);
    }

    /**
     * Stores Patient into database
     *
     * @param array $input
     *
     * @return Patient
     */
    public function store($input)
    {
        return Patient::create($input);
    }

    /**
     * Find Patient by given id
     *
     * @param int $id
     *
     * @return \Illuminate\Support\Collection|null|static|Patient
     */
    public function findPatientById($id)
    {
        return Patient::find($id);
    }


    public function findTrashedPatientById($id)
    {
        return Patient::withTrashed()->find($id);
    }

    /**
     * Updates Patient into database
     *
     * @param Patient $patient
     * @param array $input
     *
     * @return Patient
     */
    public function update($patient, $input)
    {
        $patient->fill($input);
        $patient->save();

        return $patient;
    }


    public function search($input)
    {
        $query = Patient::query();

        $columns = Schema::getColumnListing('patients');
        $attributes = array();

        foreach($columns as $attribute){
            if(!empty($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->paginate(15), $attributes];

    }
}