<?php

namespace App\Libraries\Repositories;


use App\Models\PatientBillCpt;
use Illuminate\Support\Facades\Schema;

class PatientBillCptRepository
{

	/**
	 * Returns all PatientBillCpts
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return PatientBillCpt::all();
	}

	public function search($input)
    {
        $query = PatientBillCpt::query();

        $columns = Schema::getColumnListing('patient_bill_cpts');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores PatientBillCpt into database
	 *
	 * @param array $input
	 *
	 * @return PatientBillCpt
	 */
	public function store($input)
	{
		return PatientBillCpt::create($input);
	}

	/**
	 * Find PatientBillCpt by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|PatientBillCpt
	 */
	public function findPatientBillCptById($id)
	{
		return PatientBillCpt::find($id);
	}

	/**
	 * Updates PatientBillCpt into database
	 *
	 * @param PatientBillCpt $patientBillCpt
	 * @param array $input
	 *
	 * @return PatientBillCpt
	 */
	public function update($patientBillCpt, $input)
	{
		$patientBillCpt->fill($input);
		$patientBillCpt->save();

		return $patientBillCpt;
	}
}