<?php

namespace App\Libraries\Repositories;


use App\Models\PatientPhones;

class PatientPhonesRepository
{

	/**
	 * Returns all PatientPhones
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return PatientPhones::all();
	}

	/**
	 * Stores PatientPhones into database
	 *
	 * @param array $input
	 *
	 * @return PatientPhones
	 */
	public function store($input)
	{
		return PatientPhones::create($input);
	}

	/**
	 * Find PatientPhones by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|PatientPhones
	 */
	public function findPatientPhonesById($id)
	{
		return PatientPhones::find($id);
	}

	/**
	 * Updates PatientPhones into database
	 *
	 * @param PatientPhones $patientPhones
	 * @param array $input
	 *
	 * @return PatientPhones
	 */
	public function update($patientPhones, $input)
	{
		$patientPhones->fill($input);
		$patientPhones->save();

		return $patientPhones;
	}
}