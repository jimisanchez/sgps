<?php

namespace App\Libraries\Repositories;


use App\Models\UserOrganizations;

class UserOrganizationsRepository
{

	/**
	 * Returns all UserOrganizations
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return UserOrganizations::all();
	}

	/**
	 * Stores UserOrganizations into database
	 *
	 * @param array $input
	 *
	 * @return UserOrganizations
	 */
	public function store($input)
	{
		return UserOrganizations::create($input);
	}

	/**
	 * Find UserOrganizations by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|UserOrganizations
	 */
	public function findUserOrganizationsById($id)
	{
		return UserOrganizations::find($id);
	}

	/**
	 * Updates UserOrganizations into database
	 *
	 * @param UserOrganizations $userOrganizations
	 * @param array $input
	 *
	 * @return UserOrganizations
	 */
	public function update($userOrganizations, $input)
	{
		$userOrganizations->fill($input);
		$userOrganizations->save();

		return $userOrganizations;
	}
}