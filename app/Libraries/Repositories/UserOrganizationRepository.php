<?php

namespace App\Libraries\Repositories;


use App\Models\UserOrganization;

class UserOrganizationRepository
{

	/**
	 * Returns all UserOrganizations
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return UserOrganization::all();
	}

	/**
	 * Stores UserOrganization into database
	 *
	 * @param array $input
	 *
	 * @return UserOrganization
	 */
	public function store($input)
	{
		return UserOrganization::create($input);
	}

	/**
	 * Find UserOrganization by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|UserOrganization
	 */
	public function findUserOrganizationById($id)
	{
		return UserOrganization::find($id);
	}

	/**
	 * Updates UserOrganization into database
	 *
	 * @param UserOrganization $userOrganization
	 * @param array $input
	 *
	 * @return UserOrganization
	 */
	public function update($userOrganization, $input)
	{
		$userOrganization->fill($input);
		$userOrganization->save();

		return $userOrganization;
	}
}